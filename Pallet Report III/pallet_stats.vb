﻿Public Class pallet_stats
    Implements IComparable
    Public type As String
    Public cnt As Int16
    Public Sub New(ByVal sType As String, ByVal nCnt As Int16)
        type = sType
        cnt = nCnt
    End Sub
    Public Overrides Function ToString() As String
        Return type & "  -  " & cnt
    End Function
    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Dim other_type As Type = DirectCast(obj, Type)
        Return String.Compare(Me.ToString, other_type.ToString)
    End Function
End Class
