﻿Imports System.Data.SqlClient
Imports System.Math
Imports System.Net.Mail
Imports System.Reflection

Public Class frmMain
    #Region "< Members >"
    Dim dvPallAll As New DataView
    Dim PalletList As New ArrayList
    Dim nTotalWgt As Decimal
    Dim nSkids As Int16
    Dim sShipNo As String = ""
    Dim sShipVia As String = ""
    Dim sSO As String = ""
    Const NSTDWEIGHT As Int32 = 125 ' changed std weight per cubic foot based on density tests.
    private _m2mCatalog as String
    private _psCatalog as String
    private _connM2M as String
    private _connPS As string
    private _SMTPClient as string
    #End Region

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load 
        dim appSetting = new AppSettings()

        dim sqlHelper = new SqlHelper(appSetting.GetConnectionString("M2M"))
        If (Not sqlHelper.IsConnected)
            dim frm = New frmSqlConfig
            frm.Title = "M2M"
            frm.ShowDialog(me)
        End If
        dim psSqlHelper = new SqlHelper(appSetting.GetConnectionString("PS"))
        If (Not psSqlHelper.IsConnected)
            dim frm = New frmSqlConfig
            frm.Title = "PS"
            frm.ShowDialog(me)
        End If
        _connM2M = appSetting.GetConnectionString("M2M")
        _connPS = appSetting.GetConnectionString("PS")
        
        'TODO: This line of code loads data into the 'M2mdata01DataSet.shsrce' table. You can move, or remove it, as needed.
        'Me.ShsrceTableAdapter.Fill(Me.M2mdata01DataSet.shsrce)
        'TODO: This line of code loads data into the 'M2mdata01DataSet.shipno_pickid' table. You can move, or remove it, as needed.
        'Me.Shipno_pickidTableAdapter.Fill(Me.M2mdata01DataSet.shipno_pickid)
        'TODO: This line of code loads data into the 'M2mdata01DataSet.shitem' table. You can move, or remove it, as needed.
        'Me.ShitemTableAdapter.Fill(Me.M2mdata01DataSet.shitem)
        'TODO: This line of code loads data into the 'M2mdata01DataSet.shmast' table. You can move, or remove it, as needed.
        'Me.ShmastTableAdapter.Fill(Me.M2mdata01DataSet.shmast)
        'TODO: This line of code loads data into the 'M2mdata01DataSet.pick_list' table. You can move, or remove it, as needed.
        'Me.Pick_listTableAdapter.Fill(Me.M2mdata01DataSet.pick_list)
        dvPallAll.Table = M2mdata01DataSet.pallet_tran_full

        Try
            Using ta As New m2mdata01DataSetTableAdapters.QueriesTableAdapter
                _SMTPClient = ta.spGetSMTPServerName(1).ToString.Trim
            End Using
        Catch ex As SqlException
            Using taError As New m2mdata01DataSetTableAdapters.QueriesTableAdapter
                taError.insAppErrors("Default", Me.GetType.Name, ex.Message.ToString, System.Environment.UserName)
            End Using
        End Try

        appSetting.Refersh()

        TitleReset()
    End Sub
    Private Sub txtSOSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSOSearch.TextChanged 
        If txtSOSearch.Text.Length = 6 Then
            M2mdata01DataSet.Clear()
            PalletList.Clear()
            sSO = txtSOSearch.Text.Trim.ToUpper
            Dim SoMastReader As SqlClient.SqlDataReader
            SoMastReader = get_somast(sSO)
            SoMastReader.Read()
            If Not SoMastReader.HasRows Then
                MsgBox("M2M SO was not found." & vbCrLf & "SO was not found", MsgBoxStyle.Information)
                Exit Sub
            End If
            Dim custReader As SqlClient.SqlDataReader
            custReader = get_customer(SoMastReader("fcustno"))
            custReader.Read()
            If Not custReader.HasRows Then
                MsgBox("M2M Customer was not found." & vbCrLf & "Customer was not found", MsgBoxStyle.Information)
                Exit Sub
            End If
            If custReader("fcstatus").ToString.ToUpper.Trim <> "H" Then
                cmdPicklist.Enabled = True
                fill(sSO)
            Else
                MsgBox("Customer is on credit hold", MsgBoxStyle.Exclamation, "Unable to create shippers at this time")
                cmdPicklist.Enabled = False
                fill(sSO)
            End If
            nTotalWgt = 0
            lblTotalWgt.Text = nTotalWgt
        End If
    End Sub
    Private Sub fill(ByVal sSO As String)
        Try
            SolistTableAdapter.Fill(M2mdata01DataSet.solist, sSO)
            lblOrderName.Text = find_SO(sSO)
            lblFFMID.Text = get_ffmid(sSO)

            ' get production and yarded information
            For Each dr As m2mdata01DataSet.solistRow In M2mdata01DataSet.solist
                get_jo_list(dr.fsono, dr.finumber, dr.frelease)
                Using ta As New m2mdata01DataSetTableAdapters.QueriesTableAdapter
                    dr.BeginEdit()
                    For Each drJO As m2mdata01DataSet.jo_listRow In M2mdata01DataSet.jo_list
                        dr.qty_prod = get_ht_prod(drJO.fjobno)
                        dr.yarded = get_pallet_tran(drJO.fjobno)
                    Next
                    If dr.yarded < 1 Then
                        dr.yarded = ta.spGetOnHand(dr.fac, dr.fpartno, dr.fpartrev)
                    End If

                    dr.picklistqty = ta.fnGetPickListQty(dr.fsono, dr.finumber, dr.frelease)
                    dr.EndEdit()

                End Using
                lblProgress.Text = "processing item:  " & dr.fpartno
                lblProgress.Refresh()

            Next
            M2mdata01DataSet.solist.AcceptChanges()

            lblProgress.Text = "done"
            ' do the color coding of the add check box.
            Dim nTotWgt As Decimal = 0
            Dim nRemainWgt As Decimal = 0
            With SolistDataGridView
                For Each dgvr As DataGridViewRow In .Rows
                    If .Item("dgYarded", dgvr.Index).Value = .Item("dgOrderQty", dgvr.Index).Value Then
                        ' set to green
                        ' set this first.  If it has been all shipped it will be set to black next.
                        .Rows(dgvr.Index).Cells(.Item("dgAdd", dgvr.Index).ColumnIndex).Style.BackColor = Color.LightGreen
                    End If
                    If .Item("dgOrderQty", dgvr.Index).Value - .Item("dgOnShipper", dgvr.Index).Value <= 0 Then
                        ' set color of add check box to black
                        .Rows(dgvr.Index).Cells(.Item("dgAdd", dgvr.Index).ColumnIndex).Style.BackColor = Color.Black
                    End If
                    If .Item("dgYarded", dgvr.Index).Value = 0 Then
                        ' set color to red
                        .Rows(dgvr.Index).Cells(.Item("dgAdd", dgvr.Index).ColumnIndex).Style.BackColor = Color.OrangeRed
                    End If
                    If .Item("dgYarded", dgvr.Index).Value < .Item("dgOrderQty", dgvr.Index).Value And .Item("dgYarded", dgvr.Index).Value > 0 Then
                        ' set to yellow
                        .Rows(dgvr.Index).Cells(.Item("dgAdd", dgvr.Index).ColumnIndex).Style.BackColor = Color.LightYellow
                    End If
                    nTotWgt += .Item("fnweight", dgvr.Index).Value * .Item("dgOrderQty", dgvr.Index).Value
                    nRemainWgt += .Item("fnweight", dgvr.Index).Value * (.Item("dgOrderQty", dgvr.Index).Value - .Item("dgOnShipper", dgvr.Index).Value)
                    .Item("dgLeft", dgvr.Index).Value = (.Item("dgOrderQty", dgvr.Index).Value - .Item("dgOnShipper", dgvr.Index).Value)
                    If .Item("dgLeft", dgvr.Index).Value > 0 Then
                        .Rows(dgvr.Index).Cells(.Item("dgLeft", dgvr.Index).ColumnIndex).Style.BackColor = Color.LightPink
                    End If
                Next
            End With
            lblTotOrdWgt.Text = "Tot Ord Wgt: " & FormatNumber(nTotWgt, 1)
            lblReminWgt.Text = "Remaining Wgt: " & FormatNumber(nRemainWgt, 1)

            M2mdata01DataSet.pallet_tran.Clear()
            M2mdata01DataSet.pallet_tran_full.Clear()
        Catch ex As SqlClient.SqlException
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub SolistDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles SolistDataGridView.CellEnter, SolistDataGridView.CellClick 
        With SolistDataGridView
            If e.RowIndex < 0 Then
                Exit Sub
            End If
        End With
        Dim dv As New DataView
        Dim dvPallJob As New DataView
        Dim dvPallBuild As New DataView
        Dim sPallName As String = ""
        Dim sDrawno As String
        Dim sPartno As String

        dvPallJob.Table = M2mdata01DataSet.pallet_tran
        dvPallBuild.Table = M2mdata01DataSet.pallet_build

        dv.Table = M2mdata01DataSet.jo_list
        M2mdata01DataSet.pallet_tran_full.Clear()
        M2mdata01DataSet.pallet_tran.Clear()
        Pallet_tranTableAdapter.ClearBeforeFill = False

        With SolistDataGridView
            dim abcCode as string = .Item("fabccode", e.RowIndex).Value.ToString.ToUpper.Trim

            'MsgBox(.Item("dgFprodcl", e.RowIndex).Value.ToString.Trim.ToUpper)
            'If .Item("dgFprodcl", e.RowIndex).Value.ToString.Trim.ToUpper <> "S7" Then
            If abcCode <> "X" And abcCode <> "Y" _
            And abcCode <> "Z" And abcCode <> "V" _
            And abcCode <> "W" And .Item("dgFprodcl", e.RowIndex).Value.ToString.Trim.ToUpper <> "S7" Then
                '.Item("dgFprodcl", e.RowIndex).Value.ToString.Trim.ToUpper = "7M" _
                'Or .Item("fabccode", e.RowIndex).Value.ToString.ToUpper.Trim = "V" _
                'Or .Item("dgFprodcl", e.RowIndex).Value.ToString.Trim.ToUpper = "R2" _
                'Or .Item("dgFprodcl", e.RowIndex).Value.ToString.Trim.ToUpper = "06" Then
                .Item("dgAddQty", e.RowIndex).ReadOnly = False
            Else
                If .Item("dgFprodcl", e.RowIndex).Value.ToString.Trim.ToUpper = "S7" Then
                    dv.Table = M2mdata01DataSet.saw_list
                    get_sawcuts(.Item("dgFsono", e.RowIndex).Value, .Item("dgFinumber", e.RowIndex).Value, .Item("dgFrelease", e.RowIndex).Value)
                Else
                    get_jo_list(.Item("dgFsono", e.RowIndex).Value, .Item("dgFinumber", e.RowIndex).Value, .Item("dgFrelease", e.RowIndex).Value)
                End If
                .Item("dgAddQty", e.RowIndex).ReadOnly = True
                'now that we have a list of all the travelers created for the soitem  we need to look for all the pallets it exists on.
                lblPalletList.Text = "Pallet List for SO Item: " & .Item("dgFenumber", e.RowIndex).Value
                sDrawno = .Item("dgFdrawno", e.RowIndex).Value
                sPartno = .Item("dgFpartno", e.RowIndex).Value.ToString.Trim
                For Each drv As DataRowView In dv  'jo list
                    Try
                        if abcCode = "W" or abcCode = "V" then
                           Pallet_tranTableAdapter.FillByPartno(M2mdata01DataSet.pallet_tran, sPartno)
                         else
                            Pallet_tranTableAdapter.FillByJO(M2mdata01DataSet.pallet_tran, drv("fjobno"))
                        End If
                        For Each drv2 As DataRowView In dvPallJob ' Pallet list by JO
                            Dim nTotWgt As Decimal = 0
                            Try
                                Me.Pallet_buildTableAdapter.Fill(Me.M2mdata01DataSet.pallet_build, CType(drv2("build_date"), Date), CType(drv2("seq"), Integer))
                                'dvPallBuild.Table.Clear()
                                dvPallBuild.Table = M2mdata01DataSet.pallet_build
                                Dim drFull As DataRowView
                                Dim nTempWgt As Decimal

                                For Each drv3 As DataRowView In dvPallBuild
                                    nTempWgt = drv3("wgt")
                                    If Mid(.Item("dgFprodcl", e.RowIndex).Value.ToString.Trim.ToUpper, 1, 1) = "S" Then
                                        nTempWgt = get_inboms(drv3("fpartno")) * drv3("qty")
                                    End If
                                    nTotWgt += nTempWgt
                                    drFull = dvPallAll.AddNew
                                    drFull("build_date") = drv3("build_date")
                                    drFull("seq") = drv3("seq")
                                    drFull("fjobno") = drv3("fjobno")
                                    drFull("qty") = drv3("qty")
                                    drFull("loc") = drv3("loc")
                                    drFull("bin") = drv3("bin")
                                    drFull("wgt") = nTempWgt
                                    drFull("user_id") = drv3("user_id")
                                    drFull("fscoord") = drv3("fscoord")
                                    drFull("pallet_id") = drv3("pallet_id")
                                    drFull("shipped") = drv3("shipped")
                                    drFull("updated") = drv3("updated")
                                    drFull("fac") = drv3("fac")
                                    drFull("tare_wgt") = drv3("tare_wgt")
                                    drFull("pallet_type") = drv3("pallet_type")
                                    drFull("fpartno") = drv3("fpartno")
                                    drFull("fsono") = drv3("fsono")
                                    drFull("finumber") = drv3("finumber")
                                    drFull("frelease") = drv3("frelease")
                                    drFull("piece_mark") = gen_piecemark(drv3("fpartno"))
                                    drFull("palletid") = drv2("palletid")
                                    drFull("pick_list_id") = drv2("pick_list_id")
                                    drFull("ID") = drv3("ID")
                                    Try
                                        drFull.EndEdit()
                                        M2mdata01DataSet.pallet_tran_full.AcceptChanges()
                                    Catch ex As Exception
                                        drFull.CancelEdit()

                                        'must be duplicate
                                        'ignor
                                    End Try
                                Next
                            Catch ex As System.Exception
                                System.Windows.Forms.MessageBox.Show(ex.Message)
                            End Try
                            drv2.BeginEdit()
                            If InStr(sPartno, "-") > 0 Then
                                drv2("piece_mark") = sDrawno.Trim + Mid(sPartno, InStr(sPartno, "-") - 1, sPartno.Length)
                            Else
                                drv2("piece_mark") = sDrawno.Trim
                            End If
                            drv2("wgt") = nTotWgt
                            drv2.EndEdit()
                            M2mdata01DataSet.pallet_tran.AcceptChanges()
                        Next
                    Catch ex As Exception
                        System.Windows.Forms.MessageBox.Show(ex.Message)
                    End Try
                Next
            End If
        End With
        ' reset the check boxes on both pallet datagridviews.
        Pallet_tranTableAdapter.ClearBeforeFill = True
        With Pallet_tranDataGridView
            Dim dBuild As Date
            For Each dgvr As DataGridViewRow In .Rows
                dBuild = .Item("dgptBuild_date", dgvr.Index).Value
                sPallName = dBuild.Date & " " & .Item("dgptSeq", dgvr.Index).Value.ToString
                If sSO <> .Item("dgptFsono", dgvr.Index).Value Then
                    'Messagebox.Show("mismatch SO")
                    .Item("dgptFsono", dgvr.Index).Style.BackColor = Color.Yellow
                End If
               
                If PalletList.Contains(sPallName) Then
                    'reset the pallets that have already been added.
                    .Item("dgrAddPallet", dgvr.Index).Value = True
                End If
            Next
        End With
        With Pallet_tran_fullDataGridView
            Dim dBuild As Date
            For Each dgvr As DataGridViewRow In .Rows
                dBuild = .Item("dgptfBuild_date", dgvr.Index).Value
                sPallName = dBuild.Date & " " & .Item("dgptfSeq", dgvr.Index).Value.ToString
                If PalletList.Contains(sPallName) Then
                    'reset the pallets that have already been added.
                    .Item("addpallet", dgvr.Index).Value = True
                End If
                If .Item("pick_list_id", dgvr.Index).Value <> 0 Then
                    ' it's on a picklist.  set the flag.
                    .Item("addpallet", dgvr.Index).Value = True
                End If
            Next
        End With

        dvPallBuild.Dispose()

    End Sub
    Private Function gen_piecemark(ByVal sPartno As String) As String
        Dim sDrawno As String
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select fdrawno from inmast where fpartno = '" & sPartno & "'" ' use like if we want 'S%'
            sDrawno = cmd.ExecuteScalar()
            Conn.Close()
            Conn.Dispose()
            If InStr(sPartno, "-") > 0 Then
                Return IIf(sDrawno.Trim.Length > 7, Mid(sDrawno, 1, 7), sDrawno.Trim) + Mid(sPartno, InStr(sPartno, "-"), sPartno.Length)
            Else
                Return sDrawno.Trim
            End If
        Catch ex As Exception
            Return "not found"
        End Try
    End Function
    Private Function get_inboms(ByVal sParent As String) As Decimal
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select fqty from inboms where fparent = '" & sParent & "'"
            Dim nQty As Decimal = cmd.ExecuteScalar()
            cmd.CommandText = "select fnusrqty1 from inmast where frev = '000' and fpartno = '" & Mid(sParent, 1, InStr(sParent, "-") - 1) & "'"
            Dim nCuft As Decimal = cmd.ExecuteScalar
            cmd.CommandText = "select fnweight from inmast where frev = '000' and fpartno = '" & sParent & "'"
            Dim nWeight As Decimal = cmd.ExecuteScalar
            Return IIf(nQty * nCuft * nStdWeight < nWeight, nQty * nCuft * nStdWeight, nWeight)  'return the lighter weight  ---  this will account for the future using the SolidWorks® weight of the saw cut
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Private Sub get_jo_list(ByVal sSO As String, ByVal sInumber As String, ByVal sRel As String)
        Try
            Me.Jo_listTableAdapter.Fill(Me.M2mdata01DataSet.jo_list, sSO, sInumber, sRel)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub get_sawcuts(ByVal sSO As String, ByVal sInumber As String, ByVal sRel As String)
        Try
            Me.saw_listTableAdapter.Fill(Me.M2mdata01DataSet.saw_list, sSO, sInumber, sRel)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Pallet_tranDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Pallet_tranDataGridView.CellClick 
        M2mdata01DataSet.pallet_tran_full.Clear()

        Dim dBuildDate As Date
        Dim dv As New DataView
        dv.Table = M2mdata01DataSet.pallet_info
        dv.Sort = "type"
        Dim dvPickDetail As New DataView
        dvPickDetail.Table = M2mdata01DataSet.pick_detail
        dvPickDetail.Sort = "ID"
        Dim nPTID As Int32
        Dim dvPT As New DataView
        dvPT.Table = M2mdata01DataSet.pallet_tran_full

        dvPT.Sort = "ID"
        Dim bPickVio As Boolean = False

        Dim dr As DataRowView
        Dim nRow As Integer
        Dim nSeq As Int16
        Dim bAdd As Boolean
        Dim sSo As String
        Dim sInumber As String
        Dim sRelease As String
        Dim nQty As Integer
        Dim sPallName As String = ""
        Dim sJO As String
        With Pallet_tranDataGridView
            If .Item("dgrAddPallet", e.RowIndex).ColumnIndex = e.ColumnIndex And Not .Item("dgptShipped", e.RowIndex).Value Then
                .EndEdit()
                ' forcing the value change before the object does it itself.
                .Item("dgrAddPallet", e.RowIndex).Value = Not .Item("dgrAddPallet", e.RowIndex).Value
                bAdd = .Item("dgrAddPallet", e.RowIndex).Value
                dBuildDate = .Item("dgptBuild_date", e.RowIndex).Value
                nSeq = .Item("dgptSeq", e.RowIndex).Value
                Dim nTotWgt As Decimal = .Item("dgptTare_wgt", e.RowIndex).Value + .Item("dgptWgt", e.RowIndex).Value
                If .Item("dgrAddPallet", e.RowIndex).Value Then
                    nTotalWgt += nTotWgt
                Else
                    nTotalWgt -= nTotWgt
                End If
redo:
                With Pallet_tran_fullDataGridView
                    For Each dgvr As DataGridViewRow In .Rows
                        nPTID = .Item("dgptfID", e.RowIndex).Value
                        If .Item("dgptfBuild_date", dgvr.Index).Value = dBuildDate And .Item("dgptfSeq", dgvr.Index).Value = nSeq Then
                            If .Item("pick_list_id", dgvr.Index).Value = 0 Then
                                .Item("addpallet", dgvr.Index).Value = Not .Item("addpallet", dgvr.Index).Value = True 'bAdd
                                bAdd = .Item("addpallet", dgvr.Index).Value
                                sSo = .Item("fsono", dgvr.Index).Value
                                sInumber = .Item("finumber", dgvr.Index).Value
                                sRelease = .Item("frelease", dgvr.Index).Value
                                nQty = .Item("dgptfQty", dgvr.Index).Value
                                sPallName = dBuildDate.Date & " " & nSeq.ToString
                                sJO = .Item("dgptfFjobno", dgvr.Index).Value
                                If bAdd Then
                                    'add pallet to list
                                    Dim sType As String = Pallet_tranDataGridView.Item("dgptPallet_type", e.RowIndex).Value.ToString
                                    nRow = dv.Find(Pallet_tranDataGridView.Item("dgptPallet_type", e.RowIndex).Value.ToString.Trim)
                                    If Not PalletList.Contains(sPallName) Then
                                        If nRow <> -1 Then
                                            Dim nCnt As Integer = dv.Item(nRow).Item("load_cnt")
                                            nCnt += 1
                                            dv.Item(nRow).BeginEdit()
                                            dv.Item(nRow).Item("load_cnt") = nCnt
                                            dv.Item(nRow).EndEdit()
                                        Else
                                            dr = dv.AddNew
                                            dr("type") = Pallet_tranDataGridView.Item("dgptPallet_type", e.RowIndex).Value.ToString.Trim
                                            dr("load_cnt") = 1
                                            dr.EndEdit()
                                        End If
                                        PalletList.Add(sPallName)
                                    End If
                                    ' copy pallet_tran_full to pick_detail
                                    Dim drPD As DataRow = M2mdata01DataSet.pallet_tran_full.FindBybuild_datefjobnoseq(dBuildDate, sJO, nSeq)
                                    M2mdata01DataSet.pick_detail.LoadDataRow(drPD.ItemArray, True)
                                Else
                                    ' remove pallet from list
                                    If PalletList.Contains(sPallName) Then
                                        nRow = dv.Find(Pallet_tranDataGridView.Item("dgptPallet_type", e.RowIndex).Value.ToString.Trim)
                                        If nRow <> -1 Then
                                            If dv.Item(nRow).Item("load_cnt") = 1 Then
                                                ' reduction of last pallet is a deletion 
                                                dv.Item(nRow).Delete()
                                            Else
                                                dv.Item(nRow).BeginEdit()
                                                dv.Item(nRow).Item("load_cnt") -= 1
                                                dv.Item(nRow).EndEdit()
                                            End If
                                        End If
                                        PalletList.Remove(sPallName)
                                    End If
                                    ' remove the pallet's items from pick_detail.
                                    Dim drPD As DataRow = M2mdata01DataSet.pick_detail.FindBybuild_datefjobnoseq(dBuildDate, sJO, nSeq)
                                    M2mdata01DataSet.pick_detail.Removepick_detailRow(drPD)
                                End If
                                M2mdata01DataSet.pallet_info.AcceptChanges()
                                With SolistDataGridView
                                    For Each dgvr2 As DataGridViewRow In .Rows
                                        If .Item("dgFsono", dgvr2.Index).Value = sSo And .Item("dgFinumber", dgvr2.Index).Value = sInumber And .Item("dgFrelease", dgvr2.Index).Value = sRelease Then
                                            .Item("dgAddQty", dgvr2.Index).Value += nQty * IIf(bAdd, 1, -1)
                                            .Item("dgPickList", dgvr2.Index).Value += nQty * IIf(bAdd, 1, -1)
                                            .Item("dgAdd", dgvr2.Index).Value = True
                                            If .Item("dgOrderQty", dgvr2.Index).Value = .Item("dgPickList", dgvr2.Index).Value Then
                                                .Rows(dgvr2.Index).Cells(.Item("dgOrderQty", dgvr2.Index).ColumnIndex).Style.BackColor = Color.LightGreen
                                            Else
                                                .Rows(dgvr2.Index).Cells(.Item("dgOrderQty", dgvr2.Index).ColumnIndex).Style.BackColor = Color.Yellow
                                            End If
                                            If .Item("dgAddQty", dgvr2.Index).Value = 0 Then
                                                .Item("dgAdd", dgvr2.Index).Value = False
                                                .Rows(dgvr2.Index).Cells(.Item("dgOrderQty", dgvr2.Index).ColumnIndex).Style.BackColor = .Rows(dgvr2.Index).Cells(.Item("dgAdd", dgvr2.Index).ColumnIndex).Style.BackColor
                                            End If
                                        End If
                                    Next
                                End With
                                lblTotalWgt.Text = "Total Weight:  " & Format(nTotalWgt, "#,##0.0")
                            Else
                                bPickVio = True
                            End If
                        End If
                    Next

                End With
                If bPickVio Then  ' 5/3/2012 JRM - add ability to reassociate pallet to new pick list.
                    If MsgBox("Pallet already on pick list." + vbCrLf + "Would you like to remove it from the previous pick list so it can be added to a new pick list?" _
                              + vbCrLf + "Items on pallet must be removed from M2M shipper before answering 'yes'.", MsgBoxStyle.YesNo, "multi pick list violation") = MsgBoxResult.Yes Then
                        .Item("dgrAddPallet", e.RowIndex).Value = True
                        .EndEdit()
                        Using ta As New m2mdata01DataSetTableAdapters.QueriesTableAdapter
                            Try
                                ta.UnShipPallet(dBuildDate, nSeq)
                                M2mdata01DataSet.pallet_tran_full.AcceptChanges()
                                With Pallet_tran_fullDataGridView
                                    For Each dgvr As DataGridViewRow In .Rows
                                        .Item("pick_list_id", dgvr.Index).Value = 0
                                        .Item("addpallet", dgvr.Index).Value = False
                                    Next
                                End With
                            Catch ex As Exception
                                ta.InsertAppErrors(.Item("dgptFac", e.RowIndex).Value, "Pick List Gen", ex.Message.ToString(), System.Environment.UserName)
                            End Try
                        End Using
                        bPickVio = False
                        GoTo redo
                    Else
                        .Item("dgrAddPallet", e.RowIndex).Value = False
                        .EndEdit()
                        nTotalWgt -= nTotWgt
                    End If
                End If
            Else
                If .Item("dgrAddPallet", e.RowIndex).ColumnIndex <> e.ColumnIndex Then
                    MsgBox("Must click on the Add Pallet check box to add items.", MsgBoxStyle.Information)
                Else
                    MsgBox("Pallet has already been shipped.", MsgBoxStyle.Information, "Cannot be shipped twice")
                    .Item("dgrAddPallet", e.RowIndex).Value = False
                    '  05/03/2012 JRM -  RR doesn't want this code as of yet.  only want to remove a pallet from a pick list if it hasn't been 'shipped'
                    'If MsgBox("Do you need to need to put this pallet on a shipper again?" + vbCrLf + "Answer yes, then select pallet again.", MsgBoxStyle.YesNo, "Reship?") = MsgBoxResult.Yes Then
                    '    .Item("dgptShipped", e.RowIndex).Value = False
                    '    dBuildDate = .Item("dgptBuild_date", e.RowIndex).Value
                    '    nSeq = .Item("dgptSeq", e.RowIndex).Value
                    '    With Pallet_tran_fullDataGridView
                    '        For Each dgvr As DataGridViewRow In .Rows
                    '            nPTID = .Item("dgptfID", e.RowIndex).Value
                    '            If .Item("dgptfBuild_date", dgvr.Index).Value = dBuildDate And .Item("dgptfSeq", dgvr.Index).Value = nSeq Then
                    '                .Item("pick_list_id", dgvr.Index).Value = 0
                    '                .Item("dgptfshipped", dgvr.Index).Value = False
                    '            End If
                    '        Next
                    '    End With
                    '    Using ta As New m2mdata01DataSetTableAdapters.QueriesTableAdapter
                    '        Try
                    '            ta.UnShipPallet(dBuildDate, nSeq)
                    '        Catch ex As Exception
                    '            ta.InsertAppErrors(.Item("dgptFac", e.RowIndex).Value, "Pick List Gen", ex.Message.ToString(), System.Environment.UserName)
                    '        End Try
                    '    End Using
                    'End If
                End If
            End If
        End With
        nTotalWgt = 0
        For Each drPD As m2mdata01DataSet.pick_detailRow In M2mdata01DataSet.pick_detail
            nTotalWgt += drPD.wgt
        Next
        ' count the total number of skids
        nSkids = 0
        For Each drPI As m2mdata01DataSet.pallet_infoRow In M2mdata01DataSet.pallet_info
            nSkids += drPI.load_cnt
        Next
        lblTotSkids.Text = "Skid Qty: " & nSkids

        dv.Dispose()
    End Sub
    Private Function find_SO(ByVal sSO As String) As String
        Dim Conn As New SqlClient.SqlConnection
        Conn.ConnectionString = _connM2M
        Conn.Open()
        Dim cmd As New SqlClient.SqlCommand
        cmd.Connection = Conn
        cmd.CommandText = "select fordername from somast where fsono = '" & sSO & "'" ' use like if we want 'S%'
        Return cmd.ExecuteScalar()
        Conn.Close()
        Conn.Dispose()
    End Function
    Private Function get_pick_num() As Boolean
        Dim dv As New DataView
        dv.Table = M2mdata01DataSet.pick_list
        Dim drv As DataRowView
        drv = dv.AddNew
        drv("created_by") = System.Environment.UserName
        drv("create_date") = Now
        drv("ffmid") = lblFFMID.Text.Trim
        drv("fsono") = txtSOSearch.Text.Trim.ToUpper
        drv("pu_date") = dtpPUDate.Value.Date
        drv("fshipno") = sShipNo
        Try
            drv.EndEdit()
            Pick_listTableAdapter.Update(M2mdata01DataSet.pick_list)
            M2mdata01DataSet.pick_list.AcceptChanges()
            Pick_list_idTextBox.Text = dv(dv.Count - 1).Item("pick_list_id")
            Return True
        Catch ex As Exception
            Return False
        End Try
        dv.Dispose()
    End Function
    Private Sub cmdPicklist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPicklist.Click 
        ' put the stock pallet in pallet_tran
        ' copy pallet_tran_full to pick_detail
        Cursor = Cursors.WaitCursor
        Dim dr As SqlClient.SqlDataReader = get_last_seq()
        Dim nLastSeq As Integer
        Dim sDate As String

        dr.Read()
        nLastSeq = IIf(IsDBNull(dr("seq")), 0, dr("seq"))
        sDate = dr("build_date")
        dr.Close()
        Dim dvPD As New DataView
        Dim dvPT As New DataView
        dvPT.Table = M2mdata01DataSet.pallet_tran
        Dim drFull As DataRowView

        dvPD.Table = M2mdata01DataSet.pick_detail
        Dim sPalletID As String = ""

        Pallet_tranDataGridView.SuspendLayout()

        dvPD.RowFilter = "fjobno = 'STOCK'"

        For Each drv As DataRowView In dvPD
            If drv("fjobno").ToString.Trim.ToUpper = "STOCK" Then
                nLastSeq += 1
                sPalletID = sDate & " " & nLastSeq
                PalletList.Add(sPalletID)

                drFull = dvPT.AddNew
                drFull("build_date") = sDate
                drFull("seq") = nLastSeq
                drFull("fjobno") = drv("fjobno")
                drFull("qty") = drv("qty")
                drFull("loc") = drv("loc")
                drFull("bin") = drv("bin")
                drFull("wgt") = drv("wgt")
                drFull("user_id") = drv("user_id")
                drFull("fscoord") = drv("fscoord")
                drFull("pallet_id") = drv("pallet_id")
                drFull("shipped") = drv("shipped")
                drFull("updated") = drv("updated")
                drFull("fac") = drv("fac")
                drFull("tare_wgt") = drv("tare_wgt")
                drFull("pallet_type") = drv("pallet_type")
                'drFull("fpartno") = drv("fpartno")
                drFull("fsono") = drv("fsono")
                drFull("finumber") = drv("finumber")
                drFull("frelease") = drv("frelease")
                drFull("piece_mark") = drv("piece_mark")
                drFull("palletid") = sPalletID
                drFull("pick_list_id") = drv("pick_list_id")
                drFull("boi") = 0
                Try
                    drFull.EndEdit()

                    Pallet_tranTableAdapter.Update(M2mdata01DataSet.pallet_tran)
                    M2mdata01DataSet.pallet_tran.AcceptChanges()

                Catch ex As Exception
                    MsgBox(ex.Message.ToString, MsgBoxStyle.Information, "Adding Pallets to Pallet_Tran Failed")
                End Try
            End If
        Next
        dvPD.RowFilter = Nothing

        Pallet_tranDataGridView.ResumeLayout()
        nSkids = 0
        ' count the total number of skids
        For Each drPI As m2mdata01DataSet.pallet_infoRow In M2mdata01DataSet.pallet_info
            nSkids += drPI.load_cnt
        Next

        sShipNo = get_nextShipNo() ' returns the NEXT ship number
        If sShipNo = -1 Then
            MsgBox("M2M shipper not created.", MsgBoxStyle.Information)
            Exit Sub
        End If
        ' create shipper now. before addqty is reset to 0 and we want this infront of the Preview of the Picklist.  no telling how long they will be in the report.  
        ' hate to think we have the next shipper number only to have someone else take it from us.
        create_shipper()

        ' get the next pick num by creating the next pick_list entry.
        If get_pick_num() Then
            ' now that we know the next pick_list number, we update pallet_tran and associate all pallet parts to the pick list based on the PalletList array.
            For i As Integer = 0 To PalletList.Count - 1
                Dim sID As String = PalletList(i).ToString
                Pallet_tranTableAdapter.setpicklist(Pick_list_idTextBox.Text.Trim, CType(Mid(sID, 1, InStr(sID, " ") - 1), Date), Mid(sID, InStr(sID, " ") + 1, sID.Length))
            Next
        End If

        Dim obj As New frmPickListReport
        Dim sReport As String

        obj.ds = M2mdata01DataSet
        obj.sOrderName = lblOrderName.Text.Trim
        obj.nPickListNum = Pick_list_idTextBox.Text
        obj.sFFMID = lblFFMID.Text.Trim
        obj.dPU = dtpPUDate.Value.Date
        obj.sShipNo = sShipNo
        obj.sSV = sShipVia
        obj.catalog = _psCatalog
        obj.ShowDialog(Me)
        sReport = obj.outputfile
        obj.Dispose()

        send_msg("Please see report attached of newly created Pick List/Shipper<br />Also, please remember to print M2M shipping information.<br />You can always find a copy of the pick list in \Rockcast\pick lists\", "New PL number: " & Pick_list_idTextBox.Text.Trim, sReport)

        Dim sPLID As Int16 = Pick_list_idTextBox.Text

        Dim dv As New DataView
        Pallet_tranTableAdapter.FillByPickList(M2mdata01DataSet.pallet_tran, sPLID)
        dv.Table = M2mdata01DataSet.pallet_tran
        Dim nRow As Int16
        Dim aKey(2) As Object
        dv.Sort = "fsono, finumber, frelease"
        With SolistDataGridView
            For Each dgrv As DataGridViewRow In .Rows
                aKey(0) = .Item("dgFsono", dgrv.Index).Value
                aKey(1) = .Item("dgFinumber", dgrv.Index).Value
                aKey(2) = .Item("dgFrelease", dgrv.Index).Value
                nRow = dv.Find(aKey)
                If nRow <> -1 Then
                    If .Item("fabccode", dgrv.Index).Value = "V" And .Item("dgfprodcl", dgrv.Index).Value = "7M" Then
                        .Item("dgPickList", dgrv.Index).Value += dv.Item(nRow).Item("qty")
                    End If
                    .Item("dgAdd", dgrv.Index).Value = False  ' setting it back to nothing selected for the next picklist.
                    .Item("dgAddQty", dgrv.Index).Value = 0
                End If
            Next
        End With
        ' clean the rest
        M2mdata01DataSet.pallet_tran.Clear()
        M2mdata01DataSet.pallet_tran_full.Clear()
        M2mdata01DataSet.pick_detail.Clear()
        M2mdata01DataSet.pallet_info.Clear()
        nTotalWgt = 0
        lblTotalWgt.Text = nTotalWgt
        lblTotSkids.Text = "Skid Qty: "
        'PalletList.Clear()
        Cursor = Cursors.Default
    End Sub
    Private Sub DeleteListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteListToolStripMenuItem.Click 
        Dim obj As New frmDeleteList
        obj.ShowDialog(Me)
        Dim sID As Integer = obj.sPickID
        If Not obj.bCancel Then
            Dim nResult As MsgBoxResult
            nResult = MsgBox("Are you sure you want to delete Pick List #:  " & sID & "?", MsgBoxStyle.YesNo, "Are you sure?")
            If nResult = MsgBoxResult.Yes Then
                Dim dv As New DataView
                Pallet_tranTableAdapter.FillByPickList(M2mdata01DataSet.pallet_tran, sID)
                dv.Table = M2mdata01DataSet.pallet_tran
                Dim nRow As Int16
                Dim aKey(2) As Object
                dv.Sort = "fsono, finumber, frelease"
                With SolistDataGridView
                    For Each dgrv As DataGridViewRow In .Rows
                        aKey(0) = .Item("dgFsono", dgrv.Index).Value
                        aKey(1) = .Item("dgFinumber", dgrv.Index).Value
                        aKey(2) = .Item("dgFrelease", dgrv.Index).Value
                        nRow = dv.Find(aKey)
                        If nRow <> -1 Then
                            If .Item("dgPickList", dgrv.Index).Value >= dv.Item(nRow).Item("qty") Then
                                .Item("dgPickList", dgrv.Index).Value -= dv.Item(nRow).Item("qty")
                            End If
                        End If
                    Next
                End With
                Try
                    'send message to PM to delete the associate shipper
                    Pick_listTableAdapter.FillByPickListID(M2mdata01DataSet.pick_list, sID)
                    dv.Table = M2mdata01DataSet.pick_list
                    send_msg("", "Please delete shipper " & dv.Item(0).Item("fshipno") & " since you deleted pick list " & sID, "")

                    Pallet_tranTableAdapter.clearstock(sID)
                    Pallet_tranTableAdapter.clearpicklist(sID)
                    Pick_listTableAdapter.DeleteQuery(sID)
                Catch ex As Exception

                End Try
            End If
        End If
        obj.Dispose()
        M2mdata01DataSet.pallet_tran.Clear()
        M2mdata01DataSet.pallet_tran_full.Clear()

    End Sub
    Private Sub send_msg(ByVal sBody As String, ByVal sMsg As String, ByVal sFile As String)
        Dim myMessage As New MailMessage
        Dim sFrom As String = System.Environment.UserName

        sFrom &= "@readingrock.com"

        myMessage.IsBodyHtml = True 'or MailFormat.Text 
        myMessage.Priority = MailPriority.High ', High, or Low 
        myMessage.From = New MailAddress(sFrom) 'From Address
        myMessage.To.Add(sFrom) ' jmikel@cinci.rr.com" 'Send a Blind Carbon-Copy 
        myMessage.Subject = sMsg
        If sFile.Length > 0 Then
            Dim data As New Attachment(sFile)
            myMessage.Attachments.Add(data)
            ' generate string or report
        End If

        myMessage.Body = sBody

        Dim smtp As New SmtpClient(_SMTPClient)
        smtp.Send(myMessage)

        myMessage = Nothing
    End Sub
    Private Function get_ffmid(ByVal sSO As String) As String
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select ffmid from somast_ext inner join somast on somast_ext.fkey_id = somast.identity_column where somast.fsono = '" & sSO & "'" ' use like if we want 'S%'
            Return cmd.ExecuteScalar()
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return "!found"
        End Try
    End Function
    Private Function get_ht_prod(ByVal sJO As String) As String
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connPS
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select sum(qty_prod) from ht_prod where fjobno = '" & sJO & "'" ' use like if we want 'S%'
            Return cmd.ExecuteScalar()
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Private Function get_pallet_tran(ByVal sJO As String) As String
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connPS
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select sum(qty) from pallet_tran where fjobno = '" & sJO & "'" ' use like if we want 'S%'
            Return cmd.ExecuteScalar()
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Private Sub ByPickListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByPickListToolStripMenuItem.Click 
        Dim objPR As New frmPickReport
        objPR.ShowDialog(Me)
        Dim sID As Integer = objPR.sPickID
        If Not objPR.bCancel Then
            Dim obj As New frmPickListReport
            obj.ds = M2mdata01DataSet
            obj.sOrderName = IIf(lblOrderName.Text.Trim.Length = 0, "Direct Print of PickList", lblOrderName.Text.Trim)
            obj.nPickListNum = sID
            obj.sFFMID = IIf(lblFFMID.Text.Trim.Length = 0, "", lblFFMID.Text.Trim)
            obj.ShowDialog(Me)

            obj.Dispose()
        End If
        objPR.Dispose()
    End Sub
    Private Sub BackOrderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BackOrderToolStripMenuItem.Click 
        Dim obj As New frmBackOrder
        obj.ds = M2mdata01DataSet
        obj.ShowDialog(Me)
    End Sub
    Private Sub SolistDataGridView_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles SolistDataGridView.CellEndEdit 
        Dim sUser As String = System.Environment.UserName
        'IIf(sUser.Contains("schweitzera"), MsgBox("at beginning"), Nothing)

        With SolistDataGridView
            'If .Item("dgFprodcl", e.RowIndex).Value.ToString.Substring(0, 1).ToUpper = "R" _
            'Or .Item("dgFprodcl", e.RowIndex).Value.ToString.ToUpper = "06" Then
            If .Item("dgFprodcl", e.RowIndex).Value.ToString.Substring(0, 1).ToUpper <> "S7" And 
            .Item("dgFprodcl", e.RowIndex).Value.ToString.Substring(0, 1).ToUpper <> "7M" Then
            'If .Item("dgFprodcl", e.RowIndex).Value.ToString.Substring(0, 1).ToUpper <> "S7" And _
            '.Item("fabccode", e.RowIndex).Value.ToString.Trim.ToUpper <> "X" And _
            '.Item("fabccode", e.RowIndex).Value.ToString.Trim.ToUpper <> "Y" And _
            '.Item("fabccode", e.RowIndex).Value.ToString.Trim.ToUpper <> "Z" And _
            '.Item("dgFprodcl", e.RowIndex).Value.ToString.Substring(0, 1).ToUpper <> "7M" Then
                If .Item("dgAddQty", e.RowIndex).ColumnIndex = e.ColumnIndex Then
                    If Not IsNumeric(.Item("dgAddQty", e.RowIndex).Value) Then
                        Beep()
                        '.CurrentCell(e.RowIndex, .Item("dgAddQty", e.RowIndex).ColumnIndex)
                        .Item("dgAdd", e.RowIndex).Value = False
                        Exit Sub
                    Else
                        If Val(.Item("dgAddQty", e.RowIndex).Value) > 0 Then
                            .Item("dgAdd", e.RowIndex).Value = True
                        Else
                            .Item("dgAdd", e.RowIndex).Value = False
                        End If
                    End If
                End If
            End If
            ' 10/28/2012 JRM - if it is not x, y or z, then treat as stock.  should put an entry into pallet_tran so it can be electronically shipped.
            If .Item("fabccode", e.RowIndex).Value.ToString.Trim.ToUpper <> "X" _
            And .Item("fabccode", e.RowIndex).Value.ToString.Trim.ToUpper <> "Y" _
            And .Item("fabccode", e.RowIndex).Value.ToString.Trim.ToUpper <> "Z" Then
                'IIf(sUser.Contains("schweitzera"), MsgBox("we know it is stock"), Nothing)

                If .Item("dgAddQty", e.RowIndex).ColumnIndex = e.ColumnIndex Then
                    If Not IsNumeric(.Item("dgAddQty", e.RowIndex).Value) Then
                        Beep()
                        '.CurrentCell(e.RowIndex, .Item("dgAddQty", e.RowIndex).ColumnIndex)
                        'IIf(sUser.Contains("schweitzera"), MsgBox("not numeric"), Nothing)
                        Exit Sub
                    End If
                    If .Item("dgAddQty", e.RowIndex).Value > .Item("dgYarded", e.RowIndex).Value Then
                        Dim nResult As MsgBoxResult
                        nResult = MsgBox("You are adding more than is on hand" & vbCrLf & "Are you sure?", MsgBoxStyle.YesNo, "Are you sure?")
                        If nResult = MsgBoxResult.No Then
                            .Item("dgAddQty", e.RowIndex).Value = .Item("dgYarded", e.RowIndex).Value
                            'Exit Sub
                        End If
                    End If
                    If .Item("dgAddQty", e.RowIndex).Value + .Item("dgOnShipper", e.RowIndex).Value > .Item("dgOrderQty", e.RowIndex).Value Then
                        Dim nResult As MsgBoxResult
                        nResult = MsgBox("You are adding more than is on order!" & vbCrLf & "Are you sure?", MsgBoxStyle.YesNo, "Are you sure?")
                        If nResult = MsgBoxResult.No Then
                            .Item("dgAddQty", e.RowIndex).Value = IIf(.Item("dgOrderQty", e.RowIndex).Value - .Item("dgOnShipper", e.RowIndex).Value > .Item("dgYarded", e.RowIndex).Value, + _
                                                                      .Item("dgYarded", e.RowIndex).Value, .Item("dgOrderQty", e.RowIndex).Value - .Item("dgOnShipper", e.RowIndex).Value)
                            'Exit Sub
                        End If
                    End If
                    .Item("dgAdd", e.RowIndex).Value = True
                    If .Item("dgAddQty", e.RowIndex).Value <= 0 Then
                        .Item("dgAddQty", e.RowIndex).Value = 0
                        .Item("dgAdd", e.RowIndex).Value = False
                    End If
                    'IIf(sUser.Contains("schweitzera"), MsgBox("setting up"), Nothing)

                    Dim sPart As String = .Item("dgFpartno", e.RowIndex).Value
                    Dim drStock As SqlClient.SqlDataReader
                    Dim drPalletMast As SqlClient.SqlDataReader
                    Dim drSoMast As SqlClient.SqlDataReader
                    drStock = get_stock_wgt(sPart)
                    drStock.Read()
                    drPalletMast = get_stock_pallet(IIf(Mid(sPart, 4, 1) = "0", "50x50", "40x50")) 'fourth digit in pallet determines type of material and type of pallet used.
                    drPalletMast.Read()
                    drSoMast = get_somast(SolistDataGridView.Item("dgFsono", e.RowIndex).Value)
                    drSoMast.Read()
                    Dim drSL As m2mdata01DataSet.stock_listRow
                    'drSL = M2mdata01DataSet.stock_list.Addstock_listRow(M2mdata01DataSet.stock_list.FindByfinumber(.Item("dgFinumber", e.RowIndex).Value))
                    drSL = M2mdata01DataSet.stock_list.FindByfinumber(.Item("dgFinumber", e.RowIndex).Value)

                    Dim nQtyDiff As Integer
                    Dim nSkidDiff As Integer = 0

                    If drSL Is Nothing Then
                        'IIf(sUser.Contains("schweitzera"), MsgBox("nothing in stock list. inside"), Nothing)

                        'need to add a new record to pick_detail
                        Dim drDetail As DataRowView
                        Dim dvDetail As New DataView
                        Dim dvPallInfo As New DataView
                        dvPallInfo.Table = M2mdata01DataSet.pallet_info

                        dvDetail.Table = M2mdata01DataSet.pick_detail
                        Dim sUser2 As String = System.Environment.UserName.Trim
                        If sUser2.Length > 10 Then
                            sUser2 = sUser2.Substring(0, 10)
                        End If

                        Dim nTotWgt As Decimal = (drStock("fnweight") * .Item("dgAddQty", e.RowIndex).Value) + drPalletMast("tare_wgt")
                        nTotalWgt += nTotWgt
                        drDetail = dvDetail.AddNew
                        drDetail("build_date") = Now.Date
                        drDetail("seq") = Val(.Item("dgFenumber", e.RowIndex).Value)
                        drDetail("fjobno") = "STOCK"
                        drDetail("qty") = .Item("dgAddQty", e.RowIndex).Value
                        drDetail("loc") = "WIP"
                        drDetail("bin") = "WIP"
                        drDetail("wgt") = nTotWgt
                        drDetail("user_id") = sUser2
                        drDetail("fscoord") = drSoMast("fsocoord")
                        drDetail("pallet_id") = drPalletMast("pallet_id")
                        drDetail("shipped") = False
                        drDetail("updated") = True
                        drDetail("fac") = "Default"
                        drDetail("tare_wgt") = drPalletMast("tare_wgt")
                        drDetail("pallet_type") = IIf(Mid(sPart, 4, 1) = "0", "50x50", "40x50")
                        drDetail("fpartno") = sPart
                        drDetail("fsono") = SolistDataGridView.Item("dgFsono", e.RowIndex).Value
                        drDetail("finumber") = SolistDataGridView.Item("dgFinumber", e.RowIndex).Value
                        drDetail("frelease") = SolistDataGridView.Item("dgFrelease", e.RowIndex).Value
                        drDetail("piece_mark") = gen_piecemark(sPart)
                        drDetail("palletid") = Now.Date & " " & .Item("dgFenumber", e.RowIndex).Value.ToString.Trim
                        drDetail("pick_list_id") = 0
                        Dim nRndQty As Integer = IIf(Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")) < .Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1"), + _
                                                     Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")) + 1, Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")))

                        Try
                            drDetail.EndEdit()
                            M2mdata01DataSet.pick_detail.AcceptChanges()
                            Dim drPallInfo As m2mdata01DataSet.pallet_infoRow
                            drPallInfo = M2mdata01DataSet.pallet_info.FindBytype(drPalletMast("pallet_type"))
                            If drPallInfo Is Nothing Then
                                Dim drPI As DataRowView
                                drPI = dvPallInfo.AddNew
                                drPI("type") = drPalletMast("pallet_type")
                                drPI("load_cnt") = nRndQty
                                drPI.EndEdit()
                                M2mdata01DataSet.pallet_info.AcceptChanges()
                            Else
                                drPallInfo.BeginEdit()
                                drPallInfo.load_cnt += nRndQty
                                drPallInfo.EndEdit()
                                drPallInfo.AcceptChanges()
                            End If

                            Dim dr As DataRowView
                            Dim dv As New DataView
                            drPallInfo = M2mdata01DataSet.pallet_info.FindBytype(drPalletMast("pallet_type"))

                            Dim nPallCnt As Int16 = drPallInfo.load_cnt
                            dv.Table = M2mdata01DataSet.stock_list
                            dr = dv.AddNew
                            dr("finumber") = .Item("dgFinumber", e.RowIndex).Value
                            dr("qty") = .Item("dgAddQty", e.RowIndex).Value
                            dr("cubecnt") = drStock("fcusrchr1")
                            dr("cc_onlist") = nRndQty
                            dr.EndEdit()
                            M2mdata01DataSet.stock_list.AcceptChanges()
                            dv.Dispose()
                        Catch ex As Exception
                            'IIf(sUser.Contains("schweitzera"), MsgBox(ex.Message.ToString(), MsgBoxStyle.Exclamation, "adding info"), Nothing)
                            'must be duplicate
                            'ignor
                        End Try
                        dvDetail.Dispose()
                        drStock.Close()
                        drPalletMast.Close()
                        drSoMast.Close()
                    Else
                        'we have already added. do we adjust or delete?
                        'IIf(sUser.Contains("schweitzera"), MsgBox("we think we have added something already"), Nothing)

                        If .Item("dgAddQty", e.RowIndex).Value > 0 Then
                            'already in pick_detail need to adjust according to quantity diff.
                            nQtyDiff = .Item("dgAddQty", e.RowIndex).Value - drSL("qty")
                            Dim drPD As DataRow = M2mdata01DataSet.pick_detail.FindBybuild_datefjobnoseq(Now.Date, "STOCK", .Item("dgFenumber", e.RowIndex).Value)
                            Dim nOldSkidQty As Integer = IIf(Round(drPD("qty") / drStock("fcusrchr1")) < drPD("qty") / drStock("fcusrchr1"), + _
                                                         Round(drPD("qty") / drStock("fcusrchr1")) + 1, Round(drPD("qty") / drStock("fcusrchr1")))
                            Dim nNewSkidQty As Integer = IIf(Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")) < .Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1"), + _
                             Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")) + 1, Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")))
                            nSkidDiff = nNewSkidQty - nOldSkidQty
                            Dim nRndQty As Integer = IIf(Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")) < .Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1"), + _
                             Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")) + 1, Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")))

                            ' adjust pallet count if needed.  Skiddiff may be zero
                            Dim drPallInfo As m2mdata01DataSet.pallet_infoRow
                            drPallInfo = M2mdata01DataSet.pallet_info.FindBytype(drPalletMast("pallet_type"))
                            drPallInfo.BeginEdit()
                            drPallInfo.load_cnt += nSkidDiff
                            drPallInfo.EndEdit()
                            drPallInfo.AcceptChanges()

                            drPD.BeginEdit()
                            drPD("qty") += nQtyDiff
                            drPD("wgt") += nQtyDiff * drStock("fnweight")
                            drPD.EndEdit()
                            M2mdata01DataSet.pick_detail.AcceptChanges()
                            drSL.BeginEdit()
                            drSL("qty") += nQtyDiff
                            drSL("cc_onlist") = nRndQty
                            drSL.EndEdit()
                            drSL.AcceptChanges()
                            nTotalWgt += (nQtyDiff * drStock("fnweight"))

                        Else
                            'IIf(sUser.Contains("schweitzera"), MsgBox("trying to delete"), Nothing)

                            'delete
                            nQtyDiff = .Item("dgAddQty", e.RowIndex).Value - drSL("qty")
                            Dim drPD As DataRow = M2mdata01DataSet.pick_detail.FindBybuild_datefjobnoseq(Now.Date, "STOCK", .Item("dgFenumber", e.RowIndex).Value)
                            Dim nOldSkidQty As Integer = IIf(Round(drPD("qty") / drStock("fcusrchr1")) < drPD("qty") / drStock("fcusrchr1"), + _
                                                         Round(drPD("qty") / drStock("fcusrchr1")) + 1, Round(drPD("qty") / drStock("fcusrchr1")))
                            Dim nNewSkidQty As Integer = IIf(Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")) < .Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1"), + _
                             Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")) + 1, Round(.Item("dgAddQty", e.RowIndex).Value / drStock("fcusrchr1")))
                            nSkidDiff = nNewSkidQty - nOldSkidQty

                            nTotalWgt -= (drSL("qty") * drStock("fnweight")) + drPalletMast("tare_wgt")
                            Dim drPallInfo As m2mdata01DataSet.pallet_infoRow
                            drPallInfo = M2mdata01DataSet.pallet_info.FindBytype(drPalletMast("pallet_type"))

                            If drPallInfo.load_cnt + nSkidDiff >= 1 Then
                                'reduce count by eeww.  need to rethink logic.
                                drPallInfo.BeginEdit()
                                drPallInfo.load_cnt += nSkidDiff
                                drPallInfo.EndEdit()
                                drPallInfo.AcceptChanges()
                            Else
                                ' time to delete the pallet type from list
                                M2mdata01DataSet.pallet_info.Removepallet_infoRow(drPallInfo)
                                M2mdata01DataSet.pallet_info.AcceptChanges()
                            End If
                            ' remove from pick_detail and stock_list
                            M2mdata01DataSet.stock_list.Removestock_listRow(drSL)
                            M2mdata01DataSet.pick_detail.Removepick_detailRow(drPD)
                        End If
                    End If
                End If
            End If
        End With
        lblTotalWgt.Text = "Total Weight:  " & Format(nTotalWgt, "#,##0.0")
        'IIf(sUser.Contains("schweitzera"), MsgBox("on our way out."), Nothing)

    End Sub
    Private Function get_stock_pallet(ByVal sPalletType As String) As SqlClient.SqlDataReader
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connPS
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select tare_wgt, pallet_id, pallet_type from pallet_mast where pallet_type like '" & sPalletType & "%'"
            Return cmd.ExecuteReader
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function get_somast(ByVal sSO As String) As SqlClient.SqlDataReader
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select * from somast where fsono = '" & sSO & "'"
            Return cmd.ExecuteReader
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function get_customer(ByVal sCustno As String) As SqlClient.SqlDataReader
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select * from SLCDPM where fcustno = '" & sCustno & "'"
            Return cmd.ExecuteReader
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function get_shipaddy(ByVal sCustno As String, ByVal sAddrKey As String) As SqlClient.SqlDataReader
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select * from syaddr where fcaliaskey = '" & sCustno & "' and fcaddrkey = '" & sAddrKey & "'"
            Return cmd.ExecuteReader
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function get_stock_wgt(ByVal sPart As String) As SqlClient.SqlDataReader
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select fnweight, case fcusrchr1 when '0' then '1' else isnull(fcusrchr1, 1) end fcusrchr1 from inmast where frev = '000' and fpartno = '" & sPart & "'"
            Return cmd.ExecuteReader
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function get_last_seq() As SqlClient.SqlDataReader
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connPS
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            Dim dDate As Date = Now.Date.AddDays(-1)

            If dDate.DayOfWeek = DayOfWeek.Sunday Then
                dDate = dDate.AddDays(-2)
            End If
            cmd.CommandText = "select max(seq) as seq, '" & dDate & "' as build_date from pallet_tran where build_date = '" & dDate & "'"
            Return cmd.ExecuteReader
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function get_onhand(ByVal sPart As String) As SqlClient.SqlDataReader
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select fonhand from inmast where frev = '000' and fpartno = '" & sPart & "'"
            Return cmd.ExecuteReader
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub create_shipper()
        Dim drShMast As DataRowView
        Dim drShItem As DataRowView
        Dim drShsrce As DataRowView

        Dim dvShMast As New DataView
        dvShMast.Table = M2mdata01DataSet.shmast
        Dim dvShItem As New DataView
        dvShItem.Table = M2mdata01DataSet.shitem
        Dim dvShsrce As New DataView
        dvShsrce.Table = M2mdata01DataSet.shsrce

        'Dim drSoMast As m2mdata01DataSet.somastRow
        Dim SoMastReader As SqlClient.SqlDataReader
        SoMastReader = get_somast(txtSOSearch.Text.Trim)
        SoMastReader.Read()
        If Not SoMastReader.HasRows Then
            MsgBox("M2M shipper not created." & vbCrLf & "SO was not found", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim custReader As SqlClient.SqlDataReader
        custReader = get_customer(SoMastReader("fcustno"))
        custReader.Read()
        If Not custReader.HasRows Then
            MsgBox("M2M shipper not created." & vbCrLf & "Customer was not found", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim shipaddyReader As SqlClient.SqlDataReader
        shipaddyReader = get_shipaddy(SoMastReader("fcustno"), SoMastReader("fshptoaddr"))
        shipaddyReader.Read()
        If Not shipaddyReader.HasRows Then
            MsgBox("M2M shipper not created." & vbCrLf & "Shipping address was not found", MsgBoxStyle.Information)
            Exit Sub
        End If
        sShipNo = sShipNo.PadLeft(6, "0")
        ' create shmast
        drShMast = dvShMast.AddNew

        drShMast("fbl_lading") = ""
        drShMast("fcjobno") = ""
        drShMast("fcnumber") = SoMastReader("fcustno") ' cust no from somast?
        drShMast("fcollect") = SoMastReader("fsocoord") ' fsocoord
        drShMast("fconfirm") = "N"  ' do I need a check box?  ' this is the shipped flag.
        drShMast("fcpono") = "" ' Links to POMAST whenever shmast.ftype = 'JO'
        drShMast("fcpro_id") = "" ' found as blank
        drShMast("fcsono") = SoMastReader("fsono") ' fsono
        drShMast("fcso_inum") = "" ' found blank
        drShMast("fcsono_rel") = "" ' found blank
        drShMast("fcsorev") = SoMastReader("fsorev") ' needs to come from somast
        drShMast("fcvendno") = "" ' will remain blank
        drShMast("fenter") = SoMastReader("fsocoord") ' fsocoord
        drShMast("ffob") = SoMastReader("ffob") ' OUR PLANT?  comes from somast
        drShMast("ffrtamt") = 0 ' blank
        drShMast("ffrtinvcd") = 0 ' blank
        drShMast("flisinv") = 0 ' Gets set to 1 when all shitem.fcstatus = 'C'
        drShMast("fno_boxes") = nSkids ' blank
        drShMast("fshipdate") = dtpPUDate.Value.Date
        drShMast("fshipno") = sShipNo
        sShipVia = SoMastReader("fshipvia")
        drShMast("fshipvia") = SoMastReader("fshipvia")
        drShMast("fshipwght") = nTotalWgt ' why is this blank?
        drShMast("fshptoaddr") = SoMastReader("fshptoaddr")
        drShMast("ftype") = "SO" ' this will be the type of shipper we create with this app.
        drShMast("start") = dtpPUDate.Value.Date
        drShMast("flpickprin") = 0
        drShMast("flshipprin") = 0
        drShMast("fcfname") = shipaddyReader("fcfname")
        drShMast("fclname") = shipaddyReader("fclname")
        drShMast("fccounty") = shipaddyReader("fccounty")
        drShMast("fccompany") = shipaddyReader("fccompany")
        drShMast("fccity") = shipaddyReader("fccity")
        drShMast("fccountry") = shipaddyReader("fccountry")
        drShMast("fcfax") = shipaddyReader("fcfax")
        drShMast("fcphone") = shipaddyReader("fcphone")
        drShMast("fcstate") = shipaddyReader("fcstate")
        drShMast("fczip") = shipaddyReader("fczip")
        drShMast("fporev") = "" ' blank
        drShMast("fcbcompany") = custReader("fcompany")
        drShMast("flpremcv") = 0 ' blank
        drShMast("timestamp_column") = SoMastReader("timestamp_column")
        'drShMast("identity_column") = x
        drShMast("fmreferenc") = "" ' blank 
        drShMast("fmstreet") = shipaddyReader("fmstreet")
        drShMast("fshipmemo") = "" ' blank 
        drShMast("FMTRCKNO") = "" ' blank
        drShMast("upsdate") = "01/01/1900"
        drShMast("upsaddr2") = "" ' blank
        drShMast("upsaddr3") = "" ' blank
        Try
            drShMast.EndEdit()
            ShmastTableAdapter.Update(M2mdata01DataSet.shmast)
            M2mdata01DataSet.shmast.AcceptChanges()
        Catch ex As Exception
            MsgBox(ex.Message.ToString, MsgBoxStyle.Information)
        End Try
        ' shipper item
        Dim sSoKey As String = ""
        Dim nItemNum As Int16 = 0
        Dim sItem As String
        Dim dvSoList As New DataView
        Dim nAddQty As Int16

        dvSoList.Table = M2mdata01DataSet.solist

        For Each dr As DataRow In M2mdata01DataSet.solist
            If dr.Item("add") Then
                drShItem = dvShItem.AddNew
                nItemNum += 1
                sItem = nItemNum
                sSoKey = dr.Item("fsono") & dr.Item("finumber") & dr.Item("frelease")

                nAddQty = dr.Item("AddQty")
                drShItem("fitemno") = sItem.PadLeft(6, "0")
                drShItem("fitemtype") = "" ' blank
                drShItem("fpartno") = dr.Item("fpartno")
                drShItem("frev") = dr.Item("fpartrev")
                drShItem("fenumber") = dr.Item("fenumber")
                drShItem("finvqty") = 0
                drShItem("fmeasure") = dr.Item("fmeasure") ' because this is what we work with.
                drShItem("forderqty") = dr.Item("fquantity")
                drShItem("fcpokey") = ""
                drShItem("fshipno") = sShipNo
                drShItem("fshipqty") = nAddQty
                drShItem("fsokey") = sSoKey
                drShItem("fcstatus") = "" ' not set 'C' until truck has been validated.
                drShItem("fcmiscstat") = ""
                drShItem("timestamp_column") = SoMastReader("timestamp_column")
                'drShItem("identity_column") = x
                drShItem("fmdescript") = dr.Item("fdescript")
                drShItem("fac") = dr.Item("fac")
                drShItem("sfac") = dr.Item("sfac")
                drShItem("fcudrev") = dr.Item("fcudrev")
                drShItem("IdoNo") = ""
                drShItem("QtyRecvd") = 0
                drShItem("fcustpart") = ""
                drShItem("flInvcPoss") = 0

                drShsrce = dvShsrce.AddNew
                drShsrce("fcitemno") = sItem.PadLeft(6, "0")
                drShsrce("fcshipno") = sShipNo
                drShsrce("fcshipsrce") = "IN"
                drShsrce("fljobcost") = 0
                drShsrce("fnlabcost") = 0
                drShsrce("fnmatlcost") = 0
                drShsrce("fnothrcost") = 0
                drShsrce("fnovhdcost") = 0
                drShsrce("fnshipqty") = nAddQty
                drShsrce("fnstdcost") = 0
                drShsrce("fcsrcitmno") = dr.Item("fenumber")
                'drShsrce("identity_column") = x
                drShsrce("timestamp_column") = SoMastReader("timestamp_column")
                drShsrce("fac") = ""
                ' -------------------------------------------------------------
                ' 12/9/2010 JRM added sorels.fcloc to solist query to replace hard coded TC.
                drShsrce("loc") = dr.Item("FCLOC")
                drShsrce("bin") = ""
                drShsrce("source") = "IN"
                drShsrce("fclocation") = ""
                drShsrce("sourcetype") = ""


                If set_sorels(dr.Item("fsono"), dr.Item("finumber"), dr.Item("frelease"), nAddQty) = -1 Then
                    MsgBox("failed to update SORELS.", MsgBoxStyle.Information)
                End If
                Try
                    drShsrce.EndEdit()
                    drShItem.EndEdit()
                    ShitemTableAdapter.Update(M2mdata01DataSet.shitem)
                    M2mdata01DataSet.shitem.AcceptChanges()
                    ShsrceTableAdapter.Update(M2mdata01DataSet.shsrce)
                    M2mdata01DataSet.shsrce.AcceptChanges()
                Catch ex As SqlClient.SqlException
                    App_errorsTableAdapter.Insert("TC", "Pick List Gen -- insert", ex.Message.ToString(), System.Environment.UserName, Now)
                    MsgBox(ex.Message.ToString, MsgBoxStyle.Information)
                End Try
            End If
        Next
        dvShItem.Dispose()
        dvShMast.Dispose()
        dvShsrce.Dispose()
        dvSoList.Dispose()
    End Sub
    Private Function get_nextShipNo() As String
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "select max(fshipno) + 1 from shmast"
            Return cmd.ExecuteScalar
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Private Function set_sorels(ByVal fsono As String, ByVal finumber As String, ByVal frelease As String, ByVal nAddQty As Int16) As Int16
        Try
            Dim Conn As New SqlClient.SqlConnection
            Conn.ConnectionString = _connM2M
            Conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = Conn
            cmd.CommandText = "update sorels set ftoshpbook = ftoshpbook + " & nAddQty & " where fsono = '" & fsono & "' and finumber = '" & finumber & "' and frelease = '" & frelease & "'"
            cmd.ExecuteNonQuery()
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            App_errorsTableAdapter.Insert("TC", "Pick List Gen -- update sorels", ex.Message.ToString(), System.Environment.UserName, Now)
            MsgBox(ex.Message.ToString())

            Return -1
        End Try
    End Function
    Private Sub cmdRemaining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemaining.Click 
        ' clear out anything the user has selected that he has not created as a picklist so we have a clean board to work from.
        M2mdata01DataSet.solist.RejectChanges()
        Dim nDiff As Integer = 0
        For Each dr As DataRow In M2mdata01DataSet.solist
            nDiff = dr.Item("fquantity") - dr.Item("on shipper")
            If nDiff > 0 Then
                dr.BeginEdit()
                dr.Item("AddQty") = nDiff
                dr.Item("add") = True
                dr.EndEdit()
            End If
        Next
        cmdPicklist_Click(Nothing, Nothing)

    End Sub

    Private Sub ReallocatePalletsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReallocatePalletsToolStripMenuItem.Click 
        Dim obj As New frmRepallet
        obj.sSO = txtSOSearch.Text.ToString.Trim
        obj.ShowDialog(Me)

    End Sub
    Private Sub M2MToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M2MToolStripMenuItem.Click 
        dim frm = New frmSqlConfig
        frm.Title = "M2M"
        frm.ShowDialog(me)
        If frm.Saved Then
            TitleReset()
        End If
    End Sub
    Private Sub PackingSolutionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PackingSolutionToolStripMenuItem.Click 
        dim frm = New frmSqlConfig
        frm.Title = "PS"
        frm.ShowDialog(me)
        If frm.Saved Then
            TitleReset()
        End If
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click 
        Dim obj As New AboutBox1
        obj.ShowDialog(Me)
        obj.Dispose()
    End Sub

    Private Sub ViewM2MConnectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ViewM2MConnectionToolStripMenuItem.Click 
        dim appSettings = New AppSettings()
        dim connStr = appSettings.GetConnectionString("M2M")
        Messagebox.Show(connStr)
    End Sub

    Private Sub ViewPSConnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ViewPSConnToolStripMenuItem.Click 
        dim appSettings = New AppSettings()
        dim connStr = appSettings.GetConnectionString("PS")
        Messagebox.Show(connStr)
    End Sub
    Private Function GetSource(sConnArr As String(), dSource As String) As String

        For Each el As String In sConnArr
            Dim aEl As String() = el.Split("=")
            If el.Contains("Catalog") Then
                dSource = aEl(1)
                exit for
            End If
        Next
        Return dSource
    End Function
    private Sub TitleReset
        dim appSetting = new AppSettings()
        appSetting.Refersh()

        Dim connM2M as string() = appSetting.GetConnectionString("M2M").Split(";")
        Dim connPS as string() = appSetting.GetConnectionString("PS").Split(";")
        _m2mCatalog = GetSource(connM2M, _m2mCatalog)
        _psCatalog = GetSource(connPS, _psCatalog)
        Text = "08 Pick List Gen v." + Assembly.GetExecutingAssembly().GetName().Version.ToString() + "     M2M: " + _m2mCatalog + " PS: " + _psCatalog

    End Sub


End Class
