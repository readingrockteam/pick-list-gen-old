﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPickReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPickReport))
        Me.txtPickListNum = New System.Windows.Forms.TextBox
        Me.lblPickNum = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtPickListNum
        '
        Me.txtPickListNum.Location = New System.Drawing.Point(75, 12)
        Me.txtPickListNum.Name = "txtPickListNum"
        Me.txtPickListNum.Size = New System.Drawing.Size(100, 20)
        Me.txtPickListNum.TabIndex = 0
        '
        'lblPickNum
        '
        Me.lblPickNum.AutoSize = True
        Me.lblPickNum.Location = New System.Drawing.Point(12, 15)
        Me.lblPickNum.Name = "lblPickNum"
        Me.lblPickNum.Size = New System.Drawing.Size(57, 13)
        Me.lblPickNum.TabIndex = 1
        Me.lblPickNum.Text = "Pick List #"
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(175, 55)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'frmPickReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(262, 90)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.lblPickNum)
        Me.Controls.Add(Me.txtPickListNum)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPickReport"
        Me.Text = "Pick Report by Number"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPickListNum As System.Windows.Forms.TextBox
    Friend WithEvents lblPickNum As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
End Class
