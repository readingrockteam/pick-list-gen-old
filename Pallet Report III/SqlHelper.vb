﻿Imports System.Data.SqlClient

Public Class SqlHelper
    private _cn as SqlConnection

    public sub New(conn As string)
        _cn = new SqlConnection(conn)
    End sub

    public ReadOnly Property IsConnected() As Boolean
        get
            if (_cn.State = ConnectionState.Closed)
                Try
                    _cn.Open()
                Catch ex As Exception
                    Return false
                End Try
            End If
            Return true
        End Get
    End Property
End Class