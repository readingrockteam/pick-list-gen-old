﻿Imports System.Configuration

Public Class AppSettings
    Private _config as Configuration
    public sub New()
        _config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
    End sub
    public sub Refersh()
        ConfigurationManager.RefreshSection("connectionStrings")
    End sub
    public Function GetConnectionString(key As string) As String
        if (key.Equals("M2M"))
            return _config.ConnectionStrings.ConnectionStrings("Pallet_Report_III.My.MySettings.m2mdata01ConnectionString").ConnectionString
        end if
        return _config.ConnectionStrings.ConnectionStrings("Pallet_Report_III.My.MySettings.Packing_SolutionConnectionString").ConnectionString
    End Function

    public sub SaveConnectionString(key As String, value As String)
        If (key.Equals("M2M"))
            _config.ConnectionStrings.ConnectionStrings("Pallet_Report_III.My.MySettings.m2mdata01ConnectionString").ConnectionString = value
            _config.ConnectionStrings.ConnectionStrings("Pallet_Report_III.My.MySettings.m2mdata01ConnectionString").ProviderName =
                "System.Data.SqlClient"
        end if
        If (key.Equals("PS"))
            _config.ConnectionStrings.ConnectionStrings("Pallet_Report_III.My.MySettings.Packing_SolutionConnectionString").ConnectionString = value
            _config.ConnectionStrings.ConnectionStrings("Pallet_Report_III.My.MySettings.Packing_SolutionConnectionString").ProviderName =
                "System.Data.SqlClient"
        end if
        Try
            _config.Save(ConfigurationSaveMode.Modified)
            ConfigurationManager.RefreshSection("connectionStrings")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End sub
End Class
