﻿Public Class frmSqlConfig
    Public Property Title As String
    Public Property Saved As Boolean

    Private Sub frmSqlConfig_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Text = Title
        btnOk.Enabled = false
        Saved = False
    End Sub

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        dim connStr As String = string.Format("Data Source={0};Initial Catalog=""{1}"";Trusted_Connection=yes;", txtServer.Text, txtDatabase.Text)
        Try
            dim helper As new SqlHelper(connStr)
            If (helper.IsConnected)
                MessageBox.Show("Test connection succeeded.", "Test Connection", MessageBoxButtons.OK, MessageBoxIcon.Information)
                btnOk.Enabled = true
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        dim connStr As String = string.Format("Data Source={0};Initial Catalog=""{1}"";Trusted_Connection=yes;", txtServer.Text, txtDatabase.Text)
        Try
            dim helper As new SqlHelper(connStr)
            If (helper.IsConnected)
                dim appSettings = New AppSettings()
                appSettings.SaveConnectionString(Title, connStr)
                Close()
                Saved = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub txtServer_TextChanged(sender As Object, e As EventArgs) Handles txtServer.TextChanged
        btnOk.Enabled = false
    End Sub

    Private Sub txtDatabase_TextChanged(sender As Object, e As EventArgs) Handles txtDatabase.TextChanged
        btnOk.Enabled = false
    End Sub
End Class