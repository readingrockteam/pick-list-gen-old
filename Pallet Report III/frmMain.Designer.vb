﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Pick_list_idLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.txtSOSearch = New System.Windows.Forms.TextBox()
        Me.SolistDataGridView = New System.Windows.Forms.DataGridView()
        Me.dgAdd = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgFenumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFdescript = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgOrderQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgQtyProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgYarded = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFprodcl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFinvqty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgOnShipper = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgPickList = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgLeft = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgAddQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFdrawno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFsono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFinumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFrelease = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFpartno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fabccode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fnusrqty1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fnweight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cube_cnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SolistBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.M2mdata01DataSet = New Pallet_Report_III.m2mdata01DataSet()
        Me.Pallet_tranDataGridView = New System.Windows.Forms.DataGridView()
        Me.dgrAddPallet = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgptBuild_date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptSeq = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptLoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptBin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptWgt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptPallet_type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptTare_wgt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptShipped = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgptFsono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptUser_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptFscoord = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptFjobno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptPallet_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptUpdated = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgptFac = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pallet_tranBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Pallet_tran_fullDataGridView = New System.Windows.Forms.DataGridView()
        Me.addpallet = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.fpartno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfBuild_date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfSeq = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfLoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfBin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfFjobno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfPallet_type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfWgt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfUser_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfFscoord = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfPallet_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfShipped = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgptfUpdated = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgptfFac = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfTare_wgt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fsono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.finumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.frelease = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgptfID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pick_list_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PallettranfullBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblSO = New System.Windows.Forms.Label()
        Me.lblPalletList = New System.Windows.Forms.Label()
        Me.lblPalletDetail = New System.Windows.Forms.Label()
        Me.Pallet_infoDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pallet_infoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblOrderName = New System.Windows.Forms.Label()
        Me.cmdPicklist = New System.Windows.Forms.Button()
        Me.Pick_list_idTextBox = New System.Windows.Forms.TextBox()
        Me.Pick_listBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManageListsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModifyListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpdateListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByPickListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackOrderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UtilitesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReallocatePalletsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdnubToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReconfigDBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.M2MToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewM2MConnectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PackingSolutionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewPSConnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HeloToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblFFMID = New System.Windows.Forms.Label()
        Me.dtpPUDate = New System.Windows.Forms.DateTimePicker()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.lblTotalWgt = New System.Windows.Forms.Label()
        Me.lblTotSkids = New System.Windows.Forms.Label()
        Me.lblTotOrdWgt = New System.Windows.Forms.Label()
        Me.lblReminWgt = New System.Windows.Forms.Label()
        Me.cmdRemaining = New System.Windows.Forms.Button()
        Me.ShmastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShitemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Pallet_buildBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.JoitemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SolistTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.solistTableAdapter()
        Me.TableAdapterManager = New Pallet_Report_III.m2mdata01DataSetTableAdapters.TableAdapterManager()
        Me.JoitemTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.joitemTableAdapter()
        Me.ShitemTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.shitemTableAdapter()
        Me.ShmastTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.shmastTableAdapter()
        Me.ShsrceTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.shsrceTableAdapter()
        Me.Pallet_tranTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.pallet_tranTableAdapter()
        Me.Pallet_buildTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.pallet_buildTableAdapter()
        Me.Pick_listTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.pick_listTableAdapter()
        Me.ShsrceBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Jo_listBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Jo_listTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.jo_listTableAdapter()
        Me.Saw_listBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Saw_listTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.saw_listTableAdapter()
        Me.App_errorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.App_errorsTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.app_errorsTableAdapter()
        Pick_list_idLabel = New System.Windows.Forms.Label()
        CType(Me.SolistDataGridView,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SolistBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.M2mdata01DataSet,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Pallet_tranDataGridView,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Pallet_tranBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Pallet_tran_fullDataGridView,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PallettranfullBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Pallet_infoDataGridView,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Pallet_infoBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Pick_listBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        Me.MenuStrip1.SuspendLayout
        CType(Me.ShmastBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ShitemBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Pallet_buildBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.JoitemBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ShsrceBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Jo_listBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Saw_listBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.App_errorsBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'Pick_list_idLabel
        '
        Pick_list_idLabel.AutoSize = true
        Pick_list_idLabel.Location = New System.Drawing.Point(942, 30)
        Pick_list_idLabel.Name = "Pick_list_idLabel"
        Pick_list_idLabel.Size = New System.Drawing.Size(56, 13)
        Pick_list_idLabel.TabIndex = 10
        Pick_list_idLabel.Text = "pick list id:"
        '
        'txtSOSearch
        '
        Me.txtSOSearch.Location = New System.Drawing.Point(43, 27)
        Me.txtSOSearch.Name = "txtSOSearch"
        Me.txtSOSearch.Size = New System.Drawing.Size(100, 20)
        Me.txtSOSearch.TabIndex = 0
        '
        'SolistDataGridView
        '
        Me.SolistDataGridView.AllowUserToAddRows = false
        Me.SolistDataGridView.AllowUserToDeleteRows = false
        Me.SolistDataGridView.AutoGenerateColumns = false
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SolistDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SolistDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SolistDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgAdd, Me.dgFenumber, Me.dgFdescript, Me.dgOrderQty, Me.dgQtyProd, Me.dgYarded, Me.dgFprodcl, Me.dgFinvqty, Me.dgOnShipper, Me.dgPickList, Me.dgLeft, Me.dgAddQty, Me.dgFdrawno, Me.dgFsono, Me.dgFinumber, Me.dgFrelease, Me.dgFpartno, Me.fabccode, Me.fnusrqty1, Me.fnweight, Me.cube_cnt})
        Me.SolistDataGridView.DataSource = Me.SolistBindingSource
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SolistDataGridView.DefaultCellStyle = DataGridViewCellStyle8
        Me.SolistDataGridView.Location = New System.Drawing.Point(12, 82)
        Me.SolistDataGridView.Name = "SolistDataGridView"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SolistDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.SolistDataGridView.Size = New System.Drawing.Size(986, 345)
        Me.SolistDataGridView.TabIndex = 3
        '
        'dgAdd
        '
        Me.dgAdd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgAdd.DataPropertyName = "add"
        Me.dgAdd.HeaderText = "add"
        Me.dgAdd.Name = "dgAdd"
        Me.dgAdd.ReadOnly = true
        Me.dgAdd.Width = 31
        '
        'dgFenumber
        '
        Me.dgFenumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgFenumber.DataPropertyName = "fenumber"
        Me.dgFenumber.HeaderText = "Item Num"
        Me.dgFenumber.Name = "dgFenumber"
        Me.dgFenumber.ReadOnly = true
        Me.dgFenumber.Width = 77
        '
        'dgFdescript
        '
        Me.dgFdescript.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFdescript.DataPropertyName = "fdescript"
        Me.dgFdescript.HeaderText = "Description"
        Me.dgFdescript.Name = "dgFdescript"
        Me.dgFdescript.ReadOnly = true
        Me.dgFdescript.Width = 85
        '
        'dgOrderQty
        '
        Me.dgOrderQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgOrderQty.DataPropertyName = "fquantity"
        DataGridViewCellStyle2.Format = "n0"
        Me.dgOrderQty.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgOrderQty.HeaderText = "Order Qty"
        Me.dgOrderQty.Name = "dgOrderQty"
        Me.dgOrderQty.ReadOnly = true
        Me.dgOrderQty.Width = 77
        '
        'dgQtyProd
        '
        Me.dgQtyProd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgQtyProd.DataPropertyName = "qty_prod"
        DataGridViewCellStyle3.Format = "n0"
        Me.dgQtyProd.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgQtyProd.HeaderText = "Tamped"
        Me.dgQtyProd.Name = "dgQtyProd"
        Me.dgQtyProd.ReadOnly = true
        Me.dgQtyProd.Width = 71
        '
        'dgYarded
        '
        Me.dgYarded.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgYarded.DataPropertyName = "yarded"
        DataGridViewCellStyle4.Format = "n0"
        Me.dgYarded.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgYarded.HeaderText = "Yarded"
        Me.dgYarded.Name = "dgYarded"
        Me.dgYarded.ReadOnly = true
        Me.dgYarded.Width = 66
        '
        'dgFprodcl
        '
        Me.dgFprodcl.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFprodcl.DataPropertyName = "fprodcl"
        Me.dgFprodcl.HeaderText = "Product Class"
        Me.dgFprodcl.Name = "dgFprodcl"
        Me.dgFprodcl.ReadOnly = true
        Me.dgFprodcl.Visible = false
        '
        'dgFinvqty
        '
        Me.dgFinvqty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgFinvqty.DataPropertyName = "finvqty"
        DataGridViewCellStyle5.Format = "n0"
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgFinvqty.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgFinvqty.HeaderText = "Invoiced Qty"
        Me.dgFinvqty.Name = "dgFinvqty"
        Me.dgFinvqty.ReadOnly = true
        Me.dgFinvqty.Width = 92
        '
        'dgOnShipper
        '
        Me.dgOnShipper.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgOnShipper.DataPropertyName = "on shipper"
        DataGridViewCellStyle6.Format = "n0"
        Me.dgOnShipper.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgOnShipper.HeaderText = "On Shipper"
        Me.dgOnShipper.Name = "dgOnShipper"
        Me.dgOnShipper.ReadOnly = true
        Me.dgOnShipper.Width = 85
        '
        'dgPickList
        '
        Me.dgPickList.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgPickList.DataPropertyName = "picklistqty"
        Me.dgPickList.HeaderText = "On PickList"
        Me.dgPickList.Name = "dgPickList"
        Me.dgPickList.ReadOnly = true
        Me.dgPickList.Visible = false
        '
        'dgLeft
        '
        Me.dgLeft.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.dgLeft.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgLeft.HeaderText = "Left"
        Me.dgLeft.Name = "dgLeft"
        Me.dgLeft.ReadOnly = true
        Me.dgLeft.Width = 50
        '
        'dgAddQty
        '
        Me.dgAddQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgAddQty.DataPropertyName = "AddQty"
        Me.dgAddQty.HeaderText = "Add Qty"
        Me.dgAddQty.Name = "dgAddQty"
        Me.dgAddQty.ReadOnly = true
        Me.dgAddQty.Width = 70
        '
        'dgFdrawno
        '
        Me.dgFdrawno.DataPropertyName = "fdrawno"
        Me.dgFdrawno.HeaderText = "fdrawno"
        Me.dgFdrawno.Name = "dgFdrawno"
        Me.dgFdrawno.Visible = false
        '
        'dgFsono
        '
        Me.dgFsono.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFsono.DataPropertyName = "fsono"
        Me.dgFsono.HeaderText = "SO"
        Me.dgFsono.Name = "dgFsono"
        Me.dgFsono.Visible = false
        '
        'dgFinumber
        '
        Me.dgFinumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFinumber.DataPropertyName = "finumber"
        Me.dgFinumber.HeaderText = "Item Num"
        Me.dgFinumber.Name = "dgFinumber"
        Me.dgFinumber.Visible = false
        '
        'dgFrelease
        '
        Me.dgFrelease.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFrelease.DataPropertyName = "frelease"
        Me.dgFrelease.HeaderText = "Rel"
        Me.dgFrelease.Name = "dgFrelease"
        Me.dgFrelease.Visible = false
        '
        'dgFpartno
        '
        Me.dgFpartno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFpartno.DataPropertyName = "fpartno"
        Me.dgFpartno.HeaderText = "Part"
        Me.dgFpartno.Name = "dgFpartno"
        Me.dgFpartno.Visible = false
        '
        'fabccode
        '
        Me.fabccode.DataPropertyName = "fabccode"
        Me.fabccode.HeaderText = "fabccode"
        Me.fabccode.Name = "fabccode"
        Me.fabccode.Visible = false
        '
        'fnusrqty1
        '
        Me.fnusrqty1.DataPropertyName = "fnusrqty1"
        Me.fnusrqty1.HeaderText = "fnusrqty1"
        Me.fnusrqty1.Name = "fnusrqty1"
        Me.fnusrqty1.Visible = false
        '
        'fnweight
        '
        Me.fnweight.DataPropertyName = "fnweight"
        Me.fnweight.HeaderText = "fnweight"
        Me.fnweight.Name = "fnweight"
        Me.fnweight.Visible = false
        '
        'cube_cnt
        '
        Me.cube_cnt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cube_cnt.DataPropertyName = "cube_cnt"
        Me.cube_cnt.HeaderText = "Cube Cnt"
        Me.cube_cnt.Name = "cube_cnt"
        Me.cube_cnt.ReadOnly = true
        Me.cube_cnt.Width = 76
        '
        'SolistBindingSource
        '
        Me.SolistBindingSource.DataMember = "solist"
        Me.SolistBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'M2mdata01DataSet
        '
        Me.M2mdata01DataSet.DataSetName = "m2mdata01DataSet"
        Me.M2mdata01DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Pallet_tranDataGridView
        '
        Me.Pallet_tranDataGridView.AllowUserToAddRows = false
        Me.Pallet_tranDataGridView.AllowUserToDeleteRows = false
        Me.Pallet_tranDataGridView.AutoGenerateColumns = false
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pallet_tranDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.Pallet_tranDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Pallet_tranDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgrAddPallet, Me.dgptBuild_date, Me.dgptSeq, Me.dgptQty, Me.dgptLoc, Me.dgptBin, Me.dgptWgt, Me.dgptPallet_type, Me.dgptTare_wgt, Me.dgptShipped, Me.dgptFsono, Me.dgptUser_id, Me.dgptFscoord, Me.dgptFjobno, Me.dgptPallet_id, Me.dgptUpdated, Me.dgptFac, Me.dgptID})
        Me.Pallet_tranDataGridView.DataSource = Me.Pallet_tranBindingSource
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Pallet_tranDataGridView.DefaultCellStyle = DataGridViewCellStyle13
        Me.Pallet_tranDataGridView.Location = New System.Drawing.Point(12, 449)
        Me.Pallet_tranDataGridView.Name = "Pallet_tranDataGridView"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pallet_tranDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.Pallet_tranDataGridView.Size = New System.Drawing.Size(476, 185)
        Me.Pallet_tranDataGridView.TabIndex = 4
        '
        'dgrAddPallet
        '
        Me.dgrAddPallet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgrAddPallet.DataPropertyName = "addPallet"
        Me.dgrAddPallet.HeaderText = "Add"
        Me.dgrAddPallet.Name = "dgrAddPallet"
        Me.dgrAddPallet.Width = 32
        '
        'dgptBuild_date
        '
        Me.dgptBuild_date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptBuild_date.DataPropertyName = "build_date"
        DataGridViewCellStyle11.Format = "d"
        Me.dgptBuild_date.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgptBuild_date.HeaderText = "Pallet"
        Me.dgptBuild_date.Name = "dgptBuild_date"
        Me.dgptBuild_date.Width = 58
        '
        'dgptSeq
        '
        Me.dgptSeq.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptSeq.DataPropertyName = "seq"
        Me.dgptSeq.HeaderText = "ID"
        Me.dgptSeq.Name = "dgptSeq"
        Me.dgptSeq.Width = 43
        '
        'dgptQty
        '
        Me.dgptQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptQty.DataPropertyName = "qty"
        Me.dgptQty.HeaderText = "Qty"
        Me.dgptQty.Name = "dgptQty"
        Me.dgptQty.Width = 48
        '
        'dgptLoc
        '
        Me.dgptLoc.DataPropertyName = "loc"
        Me.dgptLoc.HeaderText = "loc"
        Me.dgptLoc.Name = "dgptLoc"
        Me.dgptLoc.Visible = false
        '
        'dgptBin
        '
        Me.dgptBin.DataPropertyName = "bin"
        Me.dgptBin.HeaderText = "bin"
        Me.dgptBin.Name = "dgptBin"
        Me.dgptBin.Visible = false
        '
        'dgptWgt
        '
        Me.dgptWgt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptWgt.DataPropertyName = "wgt"
        DataGridViewCellStyle12.Format = "n2"
        Me.dgptWgt.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgptWgt.HeaderText = "Weight"
        Me.dgptWgt.Name = "dgptWgt"
        Me.dgptWgt.Width = 66
        '
        'dgptPallet_type
        '
        Me.dgptPallet_type.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptPallet_type.DataPropertyName = "pallet_type"
        Me.dgptPallet_type.HeaderText = "Pallet Size"
        Me.dgptPallet_type.Name = "dgptPallet_type"
        Me.dgptPallet_type.ReadOnly = true
        Me.dgptPallet_type.Width = 81
        '
        'dgptTare_wgt
        '
        Me.dgptTare_wgt.DataPropertyName = "tare_wgt"
        Me.dgptTare_wgt.HeaderText = "tare_wgt"
        Me.dgptTare_wgt.Name = "dgptTare_wgt"
        Me.dgptTare_wgt.Visible = false
        '
        'dgptShipped
        '
        Me.dgptShipped.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptShipped.DataPropertyName = "shipped"
        Me.dgptShipped.HeaderText = "Shipped"
        Me.dgptShipped.Name = "dgptShipped"
        Me.dgptShipped.ReadOnly = true
        Me.dgptShipped.Width = 52
        '
        'dgptFsono
        '
        Me.dgptFsono.DataPropertyName = "fsono"
        Me.dgptFsono.HeaderText = "SO"
        Me.dgptFsono.Name = "dgptFsono"
        Me.dgptFsono.ReadOnly = true
        '
        'dgptUser_id
        '
        Me.dgptUser_id.DataPropertyName = "user_id"
        Me.dgptUser_id.HeaderText = "user_id"
        Me.dgptUser_id.Name = "dgptUser_id"
        Me.dgptUser_id.Visible = false
        '
        'dgptFscoord
        '
        Me.dgptFscoord.DataPropertyName = "fscoord"
        Me.dgptFscoord.HeaderText = "fscoord"
        Me.dgptFscoord.Name = "dgptFscoord"
        Me.dgptFscoord.Visible = false
        '
        'dgptFjobno
        '
        Me.dgptFjobno.DataPropertyName = "fjobno"
        Me.dgptFjobno.HeaderText = "fjobno"
        Me.dgptFjobno.Name = "dgptFjobno"
        Me.dgptFjobno.Visible = false
        '
        'dgptPallet_id
        '
        Me.dgptPallet_id.DataPropertyName = "pallet_id"
        Me.dgptPallet_id.HeaderText = "pallet_id"
        Me.dgptPallet_id.Name = "dgptPallet_id"
        Me.dgptPallet_id.Visible = false
        '
        'dgptUpdated
        '
        Me.dgptUpdated.DataPropertyName = "updated"
        Me.dgptUpdated.HeaderText = "updated"
        Me.dgptUpdated.Name = "dgptUpdated"
        Me.dgptUpdated.Visible = false
        '
        'dgptFac
        '
        Me.dgptFac.DataPropertyName = "fac"
        Me.dgptFac.HeaderText = "fac"
        Me.dgptFac.Name = "dgptFac"
        Me.dgptFac.Visible = false
        '
        'dgptID
        '
        Me.dgptID.DataPropertyName = "ID"
        Me.dgptID.HeaderText = "ID"
        Me.dgptID.Name = "dgptID"
        Me.dgptID.ReadOnly = true
        Me.dgptID.Visible = false
        '
        'Pallet_tranBindingSource
        '
        Me.Pallet_tranBindingSource.DataMember = "pallet_tran"
        Me.Pallet_tranBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'Pallet_tran_fullDataGridView
        '
        Me.Pallet_tran_fullDataGridView.AllowUserToAddRows = false
        Me.Pallet_tran_fullDataGridView.AllowUserToDeleteRows = false
        Me.Pallet_tran_fullDataGridView.AutoGenerateColumns = false
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pallet_tran_fullDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.Pallet_tran_fullDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Pallet_tran_fullDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.addpallet, Me.fpartno, Me.dgptfBuild_date, Me.dgptfSeq, Me.dgptfQty, Me.dgptfLoc, Me.dgptfBin, Me.dgptfFjobno, Me.dgptfPallet_type, Me.dgptfWgt, Me.dgptfUser_id, Me.dgptfFscoord, Me.dgptfPallet_id, Me.dgptfShipped, Me.dgptfUpdated, Me.dgptfFac, Me.dgptfTare_wgt, Me.fsono, Me.finumber, Me.frelease, Me.dgptfID, Me.pick_list_id})
        Me.Pallet_tran_fullDataGridView.DataSource = Me.PallettranfullBindingSource
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Pallet_tran_fullDataGridView.DefaultCellStyle = DataGridViewCellStyle17
        Me.Pallet_tran_fullDataGridView.Location = New System.Drawing.Point(494, 449)
        Me.Pallet_tran_fullDataGridView.Name = "Pallet_tran_fullDataGridView"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pallet_tran_fullDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.Pallet_tran_fullDataGridView.Size = New System.Drawing.Size(622, 185)
        Me.Pallet_tran_fullDataGridView.TabIndex = 5
        '
        'addpallet
        '
        Me.addpallet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.addpallet.DataPropertyName = "addpallet"
        Me.addpallet.HeaderText = "Add"
        Me.addpallet.Name = "addpallet"
        Me.addpallet.ReadOnly = true
        Me.addpallet.Width = 32
        '
        'fpartno
        '
        Me.fpartno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.fpartno.DataPropertyName = "fpartno"
        Me.fpartno.HeaderText = "Part"
        Me.fpartno.Name = "fpartno"
        Me.fpartno.ReadOnly = true
        Me.fpartno.Width = 51
        '
        'dgptfBuild_date
        '
        Me.dgptfBuild_date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptfBuild_date.DataPropertyName = "build_date"
        Me.dgptfBuild_date.HeaderText = "Pallet"
        Me.dgptfBuild_date.Name = "dgptfBuild_date"
        Me.dgptfBuild_date.ReadOnly = true
        Me.dgptfBuild_date.Width = 58
        '
        'dgptfSeq
        '
        Me.dgptfSeq.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptfSeq.DataPropertyName = "seq"
        Me.dgptfSeq.HeaderText = "seq"
        Me.dgptfSeq.Name = "dgptfSeq"
        Me.dgptfSeq.ReadOnly = true
        Me.dgptfSeq.Width = 49
        '
        'dgptfQty
        '
        Me.dgptfQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptfQty.DataPropertyName = "qty"
        Me.dgptfQty.HeaderText = "Qty"
        Me.dgptfQty.Name = "dgptfQty"
        Me.dgptfQty.ReadOnly = true
        Me.dgptfQty.Width = 48
        '
        'dgptfLoc
        '
        Me.dgptfLoc.DataPropertyName = "loc"
        Me.dgptfLoc.HeaderText = "loc"
        Me.dgptfLoc.Name = "dgptfLoc"
        Me.dgptfLoc.ReadOnly = true
        Me.dgptfLoc.Visible = false
        '
        'dgptfBin
        '
        Me.dgptfBin.DataPropertyName = "bin"
        Me.dgptfBin.HeaderText = "bin"
        Me.dgptfBin.Name = "dgptfBin"
        Me.dgptfBin.ReadOnly = true
        Me.dgptfBin.Visible = false
        '
        'dgptfFjobno
        '
        Me.dgptfFjobno.DataPropertyName = "fjobno"
        Me.dgptfFjobno.HeaderText = "fjobno"
        Me.dgptfFjobno.Name = "dgptfFjobno"
        Me.dgptfFjobno.ReadOnly = true
        Me.dgptfFjobno.Visible = false
        '
        'dgptfPallet_type
        '
        Me.dgptfPallet_type.DataPropertyName = "pallet_type"
        Me.dgptfPallet_type.HeaderText = "pallet_type"
        Me.dgptfPallet_type.Name = "dgptfPallet_type"
        Me.dgptfPallet_type.ReadOnly = true
        Me.dgptfPallet_type.Visible = false
        '
        'dgptfWgt
        '
        Me.dgptfWgt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgptfWgt.DataPropertyName = "wgt"
        Me.dgptfWgt.HeaderText = "Weight"
        Me.dgptfWgt.Name = "dgptfWgt"
        Me.dgptfWgt.ReadOnly = true
        Me.dgptfWgt.Width = 66
        '
        'dgptfUser_id
        '
        Me.dgptfUser_id.DataPropertyName = "user_id"
        Me.dgptfUser_id.HeaderText = "user_id"
        Me.dgptfUser_id.Name = "dgptfUser_id"
        Me.dgptfUser_id.ReadOnly = true
        Me.dgptfUser_id.Visible = false
        '
        'dgptfFscoord
        '
        Me.dgptfFscoord.DataPropertyName = "fscoord"
        Me.dgptfFscoord.HeaderText = "fscoord"
        Me.dgptfFscoord.Name = "dgptfFscoord"
        Me.dgptfFscoord.ReadOnly = true
        Me.dgptfFscoord.Visible = false
        '
        'dgptfPallet_id
        '
        Me.dgptfPallet_id.DataPropertyName = "pallet_id"
        Me.dgptfPallet_id.HeaderText = "pallet_id"
        Me.dgptfPallet_id.Name = "dgptfPallet_id"
        Me.dgptfPallet_id.ReadOnly = true
        Me.dgptfPallet_id.Visible = false
        '
        'dgptfShipped
        '
        Me.dgptfShipped.DataPropertyName = "shipped"
        Me.dgptfShipped.HeaderText = "shipped"
        Me.dgptfShipped.Name = "dgptfShipped"
        Me.dgptfShipped.ReadOnly = true
        '
        'dgptfUpdated
        '
        Me.dgptfUpdated.DataPropertyName = "updated"
        Me.dgptfUpdated.HeaderText = "updated"
        Me.dgptfUpdated.Name = "dgptfUpdated"
        Me.dgptfUpdated.ReadOnly = true
        Me.dgptfUpdated.Visible = false
        '
        'dgptfFac
        '
        Me.dgptfFac.DataPropertyName = "fac"
        Me.dgptfFac.HeaderText = "fac"
        Me.dgptfFac.Name = "dgptfFac"
        Me.dgptfFac.ReadOnly = true
        Me.dgptfFac.Visible = false
        '
        'dgptfTare_wgt
        '
        Me.dgptfTare_wgt.DataPropertyName = "tare_wgt"
        Me.dgptfTare_wgt.HeaderText = "tare_wgt"
        Me.dgptfTare_wgt.Name = "dgptfTare_wgt"
        Me.dgptfTare_wgt.ReadOnly = true
        Me.dgptfTare_wgt.Visible = false
        '
        'fsono
        '
        Me.fsono.DataPropertyName = "fsono"
        Me.fsono.HeaderText = "fsono"
        Me.fsono.Name = "fsono"
        Me.fsono.ReadOnly = true
        Me.fsono.Visible = false
        '
        'finumber
        '
        Me.finumber.DataPropertyName = "finumber"
        Me.finumber.HeaderText = "finumber"
        Me.finumber.Name = "finumber"
        Me.finumber.ReadOnly = true
        Me.finumber.Visible = false
        '
        'frelease
        '
        Me.frelease.DataPropertyName = "frelease"
        Me.frelease.HeaderText = "frelease"
        Me.frelease.Name = "frelease"
        Me.frelease.ReadOnly = true
        Me.frelease.Visible = false
        '
        'dgptfID
        '
        Me.dgptfID.DataPropertyName = "ID"
        Me.dgptfID.HeaderText = "ID"
        Me.dgptfID.Name = "dgptfID"
        Me.dgptfID.Visible = false
        '
        'pick_list_id
        '
        Me.pick_list_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.pick_list_id.DataPropertyName = "pick_list_id"
        DataGridViewCellStyle16.Format = "######"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.pick_list_id.DefaultCellStyle = DataGridViewCellStyle16
        Me.pick_list_id.HeaderText = "List ID"
        Me.pick_list_id.Name = "pick_list_id"
        Me.pick_list_id.ReadOnly = true
        Me.pick_list_id.Width = 62
        '
        'PallettranfullBindingSource
        '
        Me.PallettranfullBindingSource.DataMember = "pallet_tran_full"
        Me.PallettranfullBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'lblSO
        '
        Me.lblSO.AutoSize = true
        Me.lblSO.Location = New System.Drawing.Point(12, 30)
        Me.lblSO.Name = "lblSO"
        Me.lblSO.Size = New System.Drawing.Size(25, 13)
        Me.lblSO.TabIndex = 6
        Me.lblSO.Text = "SO:"
        '
        'lblPalletList
        '
        Me.lblPalletList.AutoSize = true
        Me.lblPalletList.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic),System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblPalletList.Location = New System.Drawing.Point(12, 430)
        Me.lblPalletList.Name = "lblPalletList"
        Me.lblPalletList.Size = New System.Drawing.Size(76, 16)
        Me.lblPalletList.TabIndex = 7
        Me.lblPalletList.Text = "Pallet List"
        '
        'lblPalletDetail
        '
        Me.lblPalletDetail.AutoSize = true
        Me.lblPalletDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic),System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblPalletDetail.Location = New System.Drawing.Point(491, 430)
        Me.lblPalletDetail.Name = "lblPalletDetail"
        Me.lblPalletDetail.Size = New System.Drawing.Size(93, 16)
        Me.lblPalletDetail.TabIndex = 8
        Me.lblPalletDetail.Text = "Pallet Detail"
        '
        'Pallet_infoDataGridView
        '
        Me.Pallet_infoDataGridView.AllowUserToAddRows = false
        Me.Pallet_infoDataGridView.AllowUserToDeleteRows = false
        Me.Pallet_infoDataGridView.AllowUserToResizeRows = false
        Me.Pallet_infoDataGridView.AutoGenerateColumns = false
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pallet_infoDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.Pallet_infoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Pallet_infoDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.Pallet_infoDataGridView.DataSource = Me.Pallet_infoBindingSource
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Pallet_infoDataGridView.DefaultCellStyle = DataGridViewCellStyle21
        Me.Pallet_infoDataGridView.Location = New System.Drawing.Point(1004, 82)
        Me.Pallet_infoDataGridView.Name = "Pallet_infoDataGridView"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pallet_infoDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.Pallet_infoDataGridView.Size = New System.Drawing.Size(140, 220)
        Me.Pallet_infoDataGridView.TabIndex = 8
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "type"
        Me.DataGridViewTextBoxColumn1.Frozen = true
        Me.DataGridViewTextBoxColumn1.HeaderText = "Pallet Type"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 5
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "load_cnt"
        DataGridViewCellStyle20.Format = "n0"
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle20
        Me.DataGridViewTextBoxColumn2.HeaderText = "Load Cnt"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 75
        '
        'Pallet_infoBindingSource
        '
        Me.Pallet_infoBindingSource.DataMember = "pallet_info"
        Me.Pallet_infoBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'lblOrderName
        '
        Me.lblOrderName.AutoSize = true
        Me.lblOrderName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic),System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblOrderName.Location = New System.Drawing.Point(383, 25)
        Me.lblOrderName.Name = "lblOrderName"
        Me.lblOrderName.Size = New System.Drawing.Size(105, 20)
        Me.lblOrderName.TabIndex = 9
        Me.lblOrderName.Text = "Order Name"
        '
        'cmdPicklist
        '
        Me.cmdPicklist.Location = New System.Drawing.Point(1069, 27)
        Me.cmdPicklist.Name = "cmdPicklist"
        Me.cmdPicklist.Size = New System.Drawing.Size(75, 48)
        Me.cmdPicklist.TabIndex = 10
        Me.cmdPicklist.Text = "Create List && Shipper"
        Me.cmdPicklist.UseVisualStyleBackColor = true
        '
        'Pick_list_idTextBox
        '
        Me.Pick_list_idTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Pick_listBindingSource, "pick_list_id", true))
        Me.Pick_list_idTextBox.Enabled = false
        Me.Pick_list_idTextBox.Location = New System.Drawing.Point(1004, 27)
        Me.Pick_list_idTextBox.Name = "Pick_list_idTextBox"
        Me.Pick_list_idTextBox.Size = New System.Drawing.Size(38, 20)
        Me.Pick_list_idTextBox.TabIndex = 11
        '
        'Pick_listBindingSource
        '
        Me.Pick_listBindingSource.DataMember = "pick_list"
        Me.Pick_listBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ManageListsToolStripMenuItem, Me.ReportsToolStripMenuItem, Me.UtilitesToolStripMenuItem, Me.AdnubToolStripMenuItem, Me.HeloToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1164, 24)
        Me.MenuStrip1.TabIndex = 12
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'ManageListsToolStripMenuItem
        '
        Me.ManageListsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteListToolStripMenuItem, Me.ModifyListToolStripMenuItem, Me.UpdateListToolStripMenuItem})
        Me.ManageListsToolStripMenuItem.Name = "ManageListsToolStripMenuItem"
        Me.ManageListsToolStripMenuItem.Size = New System.Drawing.Size(88, 20)
        Me.ManageListsToolStripMenuItem.Text = "Manage Lists"
        '
        'DeleteListToolStripMenuItem
        '
        Me.DeleteListToolStripMenuItem.Name = "DeleteListToolStripMenuItem"
        Me.DeleteListToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.DeleteListToolStripMenuItem.Text = "Delete List"
        '
        'ModifyListToolStripMenuItem
        '
        Me.ModifyListToolStripMenuItem.Name = "ModifyListToolStripMenuItem"
        Me.ModifyListToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.ModifyListToolStripMenuItem.Text = "Modify List"
        '
        'UpdateListToolStripMenuItem
        '
        Me.UpdateListToolStripMenuItem.Name = "UpdateListToolStripMenuItem"
        Me.UpdateListToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.UpdateListToolStripMenuItem.Text = "Update List"
        '
        'ReportsToolStripMenuItem
        '
        Me.ReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ByPickListToolStripMenuItem, Me.BackOrderToolStripMenuItem})
        Me.ReportsToolStripMenuItem.Name = "ReportsToolStripMenuItem"
        Me.ReportsToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.ReportsToolStripMenuItem.Text = "Reports"
        '
        'ByPickListToolStripMenuItem
        '
        Me.ByPickListToolStripMenuItem.Name = "ByPickListToolStripMenuItem"
        Me.ByPickListToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
        Me.ByPickListToolStripMenuItem.Text = "by Pick List #"
        '
        'BackOrderToolStripMenuItem
        '
        Me.BackOrderToolStripMenuItem.Name = "BackOrderToolStripMenuItem"
        Me.BackOrderToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
        Me.BackOrderToolStripMenuItem.Text = "Back Order"
        '
        'UtilitesToolStripMenuItem
        '
        Me.UtilitesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReallocatePalletsToolStripMenuItem})
        Me.UtilitesToolStripMenuItem.Name = "UtilitesToolStripMenuItem"
        Me.UtilitesToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.UtilitesToolStripMenuItem.Text = "Utilites"
        '
        'ReallocatePalletsToolStripMenuItem
        '
        Me.ReallocatePalletsToolStripMenuItem.Name = "ReallocatePalletsToolStripMenuItem"
        Me.ReallocatePalletsToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.ReallocatePalletsToolStripMenuItem.Text = "reallocate pallets"
        '
        'AdnubToolStripMenuItem
        '
        Me.AdnubToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReconfigDBToolStripMenuItem})
        Me.AdnubToolStripMenuItem.Name = "AdnubToolStripMenuItem"
        Me.AdnubToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.AdnubToolStripMenuItem.Text = "Admin"
        '
        'ReconfigDBToolStripMenuItem
        '
        Me.ReconfigDBToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.M2MToolStripMenuItem, Me.PackingSolutionToolStripMenuItem})
        Me.ReconfigDBToolStripMenuItem.Name = "ReconfigDBToolStripMenuItem"
        Me.ReconfigDBToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.ReconfigDBToolStripMenuItem.Text = "Reconfig DB"
        '
        'M2MToolStripMenuItem
        '
        Me.M2MToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewM2MConnectionToolStripMenuItem})
        Me.M2MToolStripMenuItem.Name = "M2MToolStripMenuItem"
        Me.M2MToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.M2MToolStripMenuItem.Text = "M2M"
        '
        'ViewM2MConnectionToolStripMenuItem
        '
        Me.ViewM2MConnectionToolStripMenuItem.Name = "ViewM2MConnectionToolStripMenuItem"
        Me.ViewM2MConnectionToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.ViewM2MConnectionToolStripMenuItem.Text = "View M2M Conn"
        '
        'PackingSolutionToolStripMenuItem
        '
        Me.PackingSolutionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewPSConnToolStripMenuItem})
        Me.PackingSolutionToolStripMenuItem.Name = "PackingSolutionToolStripMenuItem"
        Me.PackingSolutionToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.PackingSolutionToolStripMenuItem.Text = "Packing Solution"
        '
        'ViewPSConnToolStripMenuItem
        '
        Me.ViewPSConnToolStripMenuItem.Name = "ViewPSConnToolStripMenuItem"
        Me.ViewPSConnToolStripMenuItem.Size = New System.Drawing.Size(147, 22)
        Me.ViewPSConnToolStripMenuItem.Text = "View PS Conn"
        '
        'HeloToolStripMenuItem
        '
        Me.HeloToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.HeloToolStripMenuItem.Name = "HeloToolStripMenuItem"
        Me.HeloToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HeloToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(158, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "FM ID:"
        '
        'lblFFMID
        '
        Me.lblFFMID.AutoSize = true
        Me.lblFFMID.Location = New System.Drawing.Point(203, 30)
        Me.lblFFMID.Name = "lblFFMID"
        Me.lblFFMID.Size = New System.Drawing.Size(0, 13)
        Me.lblFFMID.TabIndex = 13
        '
        'dtpPUDate
        '
        Me.dtpPUDate.Location = New System.Drawing.Point(15, 56)
        Me.dtpPUDate.Name = "dtpPUDate"
        Me.dtpPUDate.Size = New System.Drawing.Size(200, 20)
        Me.dtpPUDate.TabIndex = 14
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = true
        Me.lblProgress.Location = New System.Drawing.Point(236, 62)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(47, 13)
        Me.lblProgress.TabIndex = 15
        Me.lblProgress.Text = "progress"
        '
        'lblTotalWgt
        '
        Me.lblTotalWgt.AutoSize = true
        Me.lblTotalWgt.Location = New System.Drawing.Point(1004, 305)
        Me.lblTotalWgt.Name = "lblTotalWgt"
        Me.lblTotalWgt.Size = New System.Drawing.Size(71, 13)
        Me.lblTotalWgt.TabIndex = 16
        Me.lblTotalWgt.Text = "Total Weight:"
        '
        'lblTotSkids
        '
        Me.lblTotSkids.AutoSize = true
        Me.lblTotSkids.Location = New System.Drawing.Point(1025, 318)
        Me.lblTotSkids.Name = "lblTotSkids"
        Me.lblTotSkids.Size = New System.Drawing.Size(50, 13)
        Me.lblTotSkids.TabIndex = 16
        Me.lblTotSkids.Text = "Skid Qty:"
        '
        'lblTotOrdWgt
        '
        Me.lblTotOrdWgt.AutoSize = true
        Me.lblTotOrdWgt.Location = New System.Drawing.Point(1004, 385)
        Me.lblTotOrdWgt.Name = "lblTotOrdWgt"
        Me.lblTotOrdWgt.Size = New System.Drawing.Size(100, 13)
        Me.lblTotOrdWgt.TabIndex = 16
        Me.lblTotOrdWgt.Text = "Total Order Weight:"
        '
        'lblReminWgt
        '
        Me.lblReminWgt.AutoSize = true
        Me.lblReminWgt.Location = New System.Drawing.Point(1004, 353)
        Me.lblReminWgt.Name = "lblReminWgt"
        Me.lblReminWgt.Size = New System.Drawing.Size(112, 13)
        Me.lblReminWgt.TabIndex = 16
        Me.lblReminWgt.Text = "Order Wgt Remaining:"
        '
        'cmdRemaining
        '
        Me.cmdRemaining.Enabled = false
        Me.cmdRemaining.Location = New System.Drawing.Point(923, 53)
        Me.cmdRemaining.Name = "cmdRemaining"
        Me.cmdRemaining.Size = New System.Drawing.Size(75, 23)
        Me.cmdRemaining.TabIndex = 17
        Me.cmdRemaining.Text = "Remaining"
        Me.cmdRemaining.UseVisualStyleBackColor = true
        '
        'ShmastBindingSource
        '
        Me.ShmastBindingSource.DataMember = "shmast"
        Me.ShmastBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'ShitemBindingSource
        '
        Me.ShitemBindingSource.DataMember = "shitem"
        Me.ShitemBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'Pallet_buildBindingSource
        '
        Me.Pallet_buildBindingSource.DataMember = "pallet_build"
        Me.Pallet_buildBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'JoitemBindingSource
        '
        Me.JoitemBindingSource.DataMember = "joitem"
        Me.JoitemBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'SolistTableAdapter
        '
        Me.SolistTableAdapter.ClearBeforeFill = true
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.app_errorsTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = false
        Me.TableAdapterManager.joitemTableAdapter = Me.JoitemTableAdapter
        Me.TableAdapterManager.m2mFac_ps_xrefTableAdapter = Nothing
        Me.TableAdapterManager.pallet_tranTableAdapter = Nothing
        Me.TableAdapterManager.pick_listTableAdapter = Nothing
        Me.TableAdapterManager.picklistTableAdapter = Nothing
        Me.TableAdapterManager.shipno_pickidTableAdapter = Nothing
        Me.TableAdapterManager.shitemTableAdapter = Me.ShitemTableAdapter
        Me.TableAdapterManager.shmastTableAdapter = Me.ShmastTableAdapter
        Me.TableAdapterManager.shsrceTableAdapter = Me.ShsrceTableAdapter
        Me.TableAdapterManager.somastTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Pallet_Report_III.m2mdata01DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'JoitemTableAdapter
        '
        Me.JoitemTableAdapter.ClearBeforeFill = true
        '
        'ShitemTableAdapter
        '
        Me.ShitemTableAdapter.ClearBeforeFill = true
        '
        'ShmastTableAdapter
        '
        Me.ShmastTableAdapter.ClearBeforeFill = true
        '
        'ShsrceTableAdapter
        '
        Me.ShsrceTableAdapter.ClearBeforeFill = true
        '
        'Pallet_tranTableAdapter
        '
        Me.Pallet_tranTableAdapter.ClearBeforeFill = true
        '
        'Pallet_buildTableAdapter
        '
        Me.Pallet_buildTableAdapter.ClearBeforeFill = true
        '
        'Pick_listTableAdapter
        '
        Me.Pick_listTableAdapter.ClearBeforeFill = true
        '
        'ShsrceBindingSource
        '
        Me.ShsrceBindingSource.DataSource = Me.M2mdata01DataSet
        Me.ShsrceBindingSource.Position = 0
        '
        'Jo_listBindingSource
        '
        Me.Jo_listBindingSource.DataSource = Me.M2mdata01DataSet
        Me.Jo_listBindingSource.Position = 0
        '
        'Jo_listTableAdapter
        '
        Me.Jo_listTableAdapter.ClearBeforeFill = true
        '
        'Saw_listBindingSource
        '
        Me.Saw_listBindingSource.DataMember = "saw_list"
        Me.Saw_listBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'Saw_listTableAdapter
        '
        Me.Saw_listTableAdapter.ClearBeforeFill = true
        '
        'App_errorsBindingSource
        '
        Me.App_errorsBindingSource.DataMember = "app_errors"
        Me.App_errorsBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'App_errorsTableAdapter
        '
        Me.App_errorsTableAdapter.ClearBeforeFill = true
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1164, 654)
        Me.Controls.Add(Me.cmdRemaining)
        Me.Controls.Add(Me.lblReminWgt)
        Me.Controls.Add(Me.lblTotOrdWgt)
        Me.Controls.Add(Me.lblTotSkids)
        Me.Controls.Add(Me.lblTotalWgt)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.dtpPUDate)
        Me.Controls.Add(Me.lblFFMID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Pick_list_idLabel)
        Me.Controls.Add(Me.Pick_list_idTextBox)
        Me.Controls.Add(Me.cmdPicklist)
        Me.Controls.Add(Me.lblOrderName)
        Me.Controls.Add(Me.Pallet_infoDataGridView)
        Me.Controls.Add(Me.lblPalletDetail)
        Me.Controls.Add(Me.lblPalletList)
        Me.Controls.Add(Me.lblSO)
        Me.Controls.Add(Me.Pallet_tran_fullDataGridView)
        Me.Controls.Add(Me.Pallet_tranDataGridView)
        Me.Controls.Add(Me.SolistDataGridView)
        Me.Controls.Add(Me.txtSOSearch)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "Pick List Generator"
        CType(Me.SolistDataGridView,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SolistBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.M2mdata01DataSet,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Pallet_tranDataGridView,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Pallet_tranBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Pallet_tran_fullDataGridView,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PallettranfullBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Pallet_infoDataGridView,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Pallet_infoBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Pick_listBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        Me.MenuStrip1.ResumeLayout(false)
        Me.MenuStrip1.PerformLayout
        CType(Me.ShmastBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ShitemBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Pallet_buildBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.JoitemBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ShsrceBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Jo_listBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Saw_listBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.App_errorsBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents txtSOSearch As System.Windows.Forms.TextBox
    Friend WithEvents M2mdata01DataSet As Pallet_Report_III.m2mdata01DataSet
    Friend WithEvents SolistBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SolistTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.solistTableAdapter
    Friend WithEvents TableAdapterManager As Pallet_Report_III.m2mdata01DataSetTableAdapters.TableAdapterManager
    Friend WithEvents SolistDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents JoitemTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.joitemTableAdapter
    Friend WithEvents JoitemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Pallet_tranBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Pallet_tranTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.pallet_tranTableAdapter
    Friend WithEvents Pallet_tranDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Pallet_buildBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Pallet_buildTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.pallet_buildTableAdapter
    Friend WithEvents PallettranfullBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Pallet_tran_fullDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents dgptAddPallet As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents lblSO As System.Windows.Forms.Label
    Friend WithEvents lblPalletList As System.Windows.Forms.Label
    Friend WithEvents lblPalletDetail As System.Windows.Forms.Label
    Friend WithEvents Pallet_infoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Pallet_infoDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblOrderName As System.Windows.Forms.Label
    Friend WithEvents cmdPicklist As System.Windows.Forms.Button
    Friend WithEvents Pick_listBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Pick_listTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.pick_listTableAdapter
    Friend WithEvents Pick_list_idTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ManageListsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModifyListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblFFMID As System.Windows.Forms.Label
    Friend WithEvents dtpPUDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents lblTotalWgt As System.Windows.Forms.Label
    Friend WithEvents ReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByPickListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackOrderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShmastBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ShmastTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.shmastTableAdapter
    Friend WithEvents ShitemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ShitemTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.shitemTableAdapter
    Friend WithEvents ShsrceBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ShsrceTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.shsrceTableAdapter
    Friend WithEvents lblTotSkids As System.Windows.Forms.Label
    Friend WithEvents Jo_listBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Jo_listTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.jo_listTableAdapter
    Friend WithEvents lblTotOrdWgt As System.Windows.Forms.Label
    Friend WithEvents lblReminWgt As System.Windows.Forms.Label
    Friend WithEvents cmdRemaining As System.Windows.Forms.Button
    Friend WithEvents Saw_listBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Saw_listTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.saw_listTableAdapter
    Friend WithEvents UtilitesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReallocatePalletsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgAdd As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgFenumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFdescript As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgOrderQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgQtyProd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgYarded As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFprodcl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFinvqty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgOnShipper As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgPickList As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgLeft As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgAddQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFdrawno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFsono As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFinumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFrelease As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFpartno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fabccode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fnusrqty1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fnweight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cube_cnt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AdnubToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReconfigDBToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents M2MToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PackingSolutionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents App_errorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents App_errorsTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.app_errorsTableAdapter
    Friend WithEvents HeloToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents addpallet As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents fpartno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfBuild_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfSeq As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfLoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfBin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfFjobno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfPallet_type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfWgt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfUser_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfFscoord As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfPallet_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfShipped As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgptfUpdated As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgptfFac As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfTare_wgt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fsono As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents finumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents frelease As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgptfID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pick_list_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ViewM2MConnectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ViewPSConnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents dgrAddPallet As DataGridViewCheckBoxColumn
    Friend WithEvents dgptBuild_date As DataGridViewTextBoxColumn
    Friend WithEvents dgptSeq As DataGridViewTextBoxColumn
    Friend WithEvents dgptQty As DataGridViewTextBoxColumn
    Friend WithEvents dgptLoc As DataGridViewTextBoxColumn
    Friend WithEvents dgptBin As DataGridViewTextBoxColumn
    Friend WithEvents dgptWgt As DataGridViewTextBoxColumn
    Friend WithEvents dgptPallet_type As DataGridViewTextBoxColumn
    Friend WithEvents dgptTare_wgt As DataGridViewTextBoxColumn
    Friend WithEvents dgptShipped As DataGridViewCheckBoxColumn
    Friend WithEvents dgptFsono As DataGridViewTextBoxColumn
    Friend WithEvents dgptUser_id As DataGridViewTextBoxColumn
    Friend WithEvents dgptFscoord As DataGridViewTextBoxColumn
    Friend WithEvents dgptFjobno As DataGridViewTextBoxColumn
    Friend WithEvents dgptPallet_id As DataGridViewTextBoxColumn
    Friend WithEvents dgptUpdated As DataGridViewCheckBoxColumn
    Friend WithEvents dgptFac As DataGridViewTextBoxColumn
    Friend WithEvents dgptID As DataGridViewTextBoxColumn
End Class
