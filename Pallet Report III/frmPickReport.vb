﻿Public Class frmPickReport
    Public sPickID As Int16
    Public bCancel As Boolean = True

    Private Sub txtPickListNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPickListNum.KeyPress
        If e.KeyChar = ChrW(Keys.Return) Then
            If IsNumeric(txtPickListNum.Text) Then
                sPickID = txtPickListNum.Text
                bCancel = False
                Me.Close()
            Else
                txtPickListNum.Focus()
                txtPickListNum.Clear()
                Beep()
            End If
        End If
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

End Class