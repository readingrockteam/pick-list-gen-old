﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRepallet
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRepallet))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.RepalletDataGridView = New System.Windows.Forms.DataGridView
        Me.dgChange = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RepalletBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.M2mdata01DataSet = New Pallet_Report_III.m2mdata01DataSet
        Me.RepalletTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.repalletTableAdapter
        Me.TableAdapterManager = New Pallet_Report_III.m2mdata01DataSetTableAdapters.TableAdapterManager
        Me.Repallet_detailBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Repallet_detailTableAdapter = New Pallet_Report_III.m2mdata01DataSetTableAdapters.repallet_detailTableAdapter
        Me.Repallet_detailBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Repallet_detailDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.RepalletDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepalletBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.M2mdata01DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Repallet_detailBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Repallet_detailBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Repallet_detailDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(538, 630)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'RepalletDataGridView
        '
        Me.RepalletDataGridView.AllowUserToAddRows = False
        Me.RepalletDataGridView.AllowUserToDeleteRows = False
        Me.RepalletDataGridView.AutoGenerateColumns = False
        Me.RepalletDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.RepalletDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgChange, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.RepalletDataGridView.DataSource = Me.RepalletBindingSource
        Me.RepalletDataGridView.Location = New System.Drawing.Point(12, 68)
        Me.RepalletDataGridView.Name = "RepalletDataGridView"
        Me.RepalletDataGridView.Size = New System.Drawing.Size(317, 588)
        Me.RepalletDataGridView.TabIndex = 3
        '
        'dgChange
        '
        Me.dgChange.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgChange.DataPropertyName = "select"
        Me.dgChange.HeaderText = "Select"
        Me.dgChange.Name = "dgChange"
        Me.dgChange.Width = 43
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "build_date"
        DataGridViewCellStyle1.Format = "d"
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 55
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "seq"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Seq"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 51
        '
        'RepalletBindingSource
        '
        Me.RepalletBindingSource.DataMember = "repallet"
        Me.RepalletBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'M2mdata01DataSet
        '
        Me.M2mdata01DataSet.DataSetName = "m2mdata01DataSet"
        Me.M2mdata01DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RepalletTableAdapter
        '
        Me.RepalletTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.joitemTableAdapter = Nothing
        Me.TableAdapterManager.pallet_tranTableAdapter = Nothing
        Me.TableAdapterManager.pick_listTableAdapter = Nothing
        Me.TableAdapterManager.picklistTableAdapter = Nothing
        Me.TableAdapterManager.shipno_pickidTableAdapter = Nothing
        Me.TableAdapterManager.shitemTableAdapter = Nothing
        Me.TableAdapterManager.shmastTableAdapter = Nothing
        Me.TableAdapterManager.shsrceTableAdapter = Nothing
        Me.TableAdapterManager.somastTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Pallet_Report_III.m2mdata01DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Repallet_detailBindingSource
        '
        Me.Repallet_detailBindingSource.DataMember = "repallet_detail"
        Me.Repallet_detailBindingSource.DataSource = Me.M2mdata01DataSet
        '
        'Repallet_detailTableAdapter
        '
        Me.Repallet_detailTableAdapter.ClearBeforeFill = True
        '
        'Repallet_detailBindingSource1
        '
        Me.Repallet_detailBindingSource1.DataMember = "repallet_repallet_detail"
        Me.Repallet_detailBindingSource1.DataSource = Me.RepalletBindingSource
        '
        'Repallet_detailDataGridView
        '
        Me.Repallet_detailDataGridView.AllowUserToAddRows = False
        Me.Repallet_detailDataGridView.AllowUserToDeleteRows = False
        Me.Repallet_detailDataGridView.AutoGenerateColumns = False
        Me.Repallet_detailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Repallet_detailDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.Repallet_detailDataGridView.DataSource = Me.Repallet_detailBindingSource1
        Me.Repallet_detailDataGridView.Location = New System.Drawing.Point(335, 68)
        Me.Repallet_detailDataGridView.Name = "Repallet_detailDataGridView"
        Me.Repallet_detailDataGridView.Size = New System.Drawing.Size(350, 556)
        Me.Repallet_detailDataGridView.TabIndex = 4
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "fdescript"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 85
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "qty"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Qty"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 48
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "fsono"
        Me.DataGridViewTextBoxColumn5.HeaderText = "SO"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 47
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "finumber"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Line"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 52
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "build_date"
        Me.DataGridViewTextBoxColumn7.HeaderText = "build_date"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "seq"
        Me.DataGridViewTextBoxColumn8.HeaderText = "seq"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'frmRepallet
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(696, 671)
        Me.Controls.Add(Me.Repallet_detailDataGridView)
        Me.Controls.Add(Me.RepalletDataGridView)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRepallet"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reallocate Pallets"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.RepalletDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepalletBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.M2mdata01DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Repallet_detailBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Repallet_detailBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Repallet_detailDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents M2mdata01DataSet As Pallet_Report_III.m2mdata01DataSet
    Friend WithEvents RepalletBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RepalletTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.repalletTableAdapter
    Friend WithEvents TableAdapterManager As Pallet_Report_III.m2mdata01DataSetTableAdapters.TableAdapterManager
    Friend WithEvents RepalletDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents dgChange As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Repallet_detailBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Repallet_detailTableAdapter As Pallet_Report_III.m2mdata01DataSetTableAdapters.repallet_detailTableAdapter
    Friend WithEvents Repallet_detailBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Repallet_detailDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
