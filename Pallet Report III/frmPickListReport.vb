﻿Public Class frmPickListReport
    Public ds As DataSet
    Dim rpt As rptPickList
    Public sOrderName As String
    Public nPickListNum As Integer
    Public sFFMID As String
    Public dPU As Date
    Public sShipNo As String
    Public outputfile As String
    Public sSV As String
    Public catalog as string


    Private Sub frmPickListReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "rptPickList.rpt"
        rpt = New rptPickList
        'MsgBox()
        'rpt.FileName = Application.StartupPath & "\rptPickList.rpt"
        rpt.FileName = "\\rrs0009\public\RRPRS\Packing Solutions\crystal reports\08\rptPickList.rpt"
        rpt.SetDataSource(ds)
        rpt.SetParameterValue("picknum", nPickListNum)
        rpt.SetParameterValue("ordername", sOrderName)
        rpt.SetParameterValue("ffmid", sFFMID)
        rpt.SetParameterValue("pickup_dt", dPU)
        rpt.SetParameterValue("shipno", sShipNo)
        rpt.SetParameterValue("shipvia", sSV)
        crvReport.ReportSource = rpt

        dim plFolder as String = "\\rrs0009\public\operations\rockcast\pick lists\" & catalog.Replace(Chr(34), "")
        If(Not System.IO.Directory.Exists(plFolder)) Then
            System.IO.Directory.CreateDirectory(plFolder)
        End If
        outputfile = "\\rrs0009\public\operations\rockcast\pick lists\" & catalog.Replace(Chr(34), "") & "\" & nPickListNum & ".pdf"
        rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, outputfile)

    End Sub
End Class