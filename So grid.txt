         Me.dgAdd = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgFenumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFdescript = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgOrderQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgQtyProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgYarded = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFprodcl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFinvqty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgOnShipper = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgPickList = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgLeft = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgAddQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFdrawno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFsono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFinumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFrelease = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgFpartno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fabccode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fnusrqty1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fnweight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cube_cnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
 
 '
        'SolistDataGridView
        '
        Me.SolistDataGridView.AllowUserToAddRows = false
        Me.SolistDataGridView.AllowUserToDeleteRows = false
        Me.SolistDataGridView.AutoGenerateColumns = false
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SolistDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SolistDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SolistDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgAdd, Me.dgFenumber, Me.dgFdescript, Me.dgOrderQty, Me.dgQtyProd, Me.dgYarded, Me.dgFprodcl, Me.dgFinvqty, Me.dgOnShipper, Me.dgPickList, Me.dgLeft, Me.dgAddQty, Me.dgFdrawno, Me.dgFsono, Me.dgFinumber, Me.dgFrelease, Me.dgFpartno, Me.fabccode, Me.fnusrqty1, Me.fnweight, Me.cube_cnt})
        Me.SolistDataGridView.DataSource = Me.Solist2BindingSource
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SolistDataGridView.DefaultCellStyle = DataGridViewCellStyle8
        Me.SolistDataGridView.Location = New System.Drawing.Point(12, 82)
        Me.SolistDataGridView.Name = "SolistDataGridView"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SolistDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.SolistDataGridView.Size = New System.Drawing.Size(986, 345)
        Me.SolistDataGridView.TabIndex = 3
        '
        'dgAdd
        '
        Me.dgAdd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgAdd.DataPropertyName = "add"
        Me.dgAdd.HeaderText = "add"
        Me.dgAdd.Name = "dgAdd"
        Me.dgAdd.ReadOnly = true
        Me.dgAdd.Width = 31
        '
        'dgFenumber
        '
        Me.dgFenumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgFenumber.DataPropertyName = "fenumber"
        Me.dgFenumber.HeaderText = "Item Num"
        Me.dgFenumber.Name = "dgFenumber"
        Me.dgFenumber.ReadOnly = true
        Me.dgFenumber.Width = 77
        '
        'dgFdescript
        '
        Me.dgFdescript.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFdescript.DataPropertyName = "fdescript"
        Me.dgFdescript.HeaderText = "Description"
        Me.dgFdescript.Name = "dgFdescript"
        Me.dgFdescript.ReadOnly = true
        Me.dgFdescript.Width = 85
        '
        'dgOrderQty
        '
        Me.dgOrderQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgOrderQty.DataPropertyName = "fquantity"
        DataGridViewCellStyle2.Format = "n0"
        Me.dgOrderQty.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgOrderQty.HeaderText = "Order Qty"
        Me.dgOrderQty.Name = "dgOrderQty"
        Me.dgOrderQty.ReadOnly = true
        Me.dgOrderQty.Width = 77
        '
        'dgQtyProd
        '
        Me.dgQtyProd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgQtyProd.DataPropertyName = "qty_prod"
        DataGridViewCellStyle3.Format = "n0"
        Me.dgQtyProd.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgQtyProd.HeaderText = "Tamped"
        Me.dgQtyProd.Name = "dgQtyProd"
        Me.dgQtyProd.ReadOnly = true
        Me.dgQtyProd.Width = 71
        '
        'dgYarded
        '
        Me.dgYarded.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgYarded.DataPropertyName = "yarded"
        DataGridViewCellStyle4.Format = "n0"
        Me.dgYarded.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgYarded.HeaderText = "Yarded"
        Me.dgYarded.Name = "dgYarded"
        Me.dgYarded.ReadOnly = true
        Me.dgYarded.Width = 66
        '
        'dgFprodcl
        '
        Me.dgFprodcl.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFprodcl.DataPropertyName = "fprodcl"
        Me.dgFprodcl.HeaderText = "Product Class"
        Me.dgFprodcl.Name = "dgFprodcl"
        Me.dgFprodcl.ReadOnly = true
        Me.dgFprodcl.Visible = false
        '
        'dgFinvqty
        '
        Me.dgFinvqty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgFinvqty.DataPropertyName = "finvqty"
        DataGridViewCellStyle5.Format = "n0"
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgFinvqty.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgFinvqty.HeaderText = "Invoiced Qty"
        Me.dgFinvqty.Name = "dgFinvqty"
        Me.dgFinvqty.ReadOnly = true
        Me.dgFinvqty.Width = 92
        '
        'dgOnShipper
        '
        Me.dgOnShipper.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgOnShipper.DataPropertyName = "on shipper"
        DataGridViewCellStyle6.Format = "n0"
        Me.dgOnShipper.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgOnShipper.HeaderText = "On Shipper"
        Me.dgOnShipper.Name = "dgOnShipper"
        Me.dgOnShipper.ReadOnly = true
        Me.dgOnShipper.Width = 85
        '
        'dgPickList
        '
        Me.dgPickList.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgPickList.DataPropertyName = "picklistqty"
        Me.dgPickList.HeaderText = "On PickList"
        Me.dgPickList.Name = "dgPickList"
        Me.dgPickList.ReadOnly = true
        Me.dgPickList.Visible = false
        '
        'dgLeft
        '
        Me.dgLeft.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.dgLeft.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgLeft.HeaderText = "Left"
        Me.dgLeft.Name = "dgLeft"
        Me.dgLeft.ReadOnly = true
        Me.dgLeft.Width = 50
        '
        'dgAddQty
        '
        Me.dgAddQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgAddQty.DataPropertyName = "AddQty"
        Me.dgAddQty.HeaderText = "Add Qty"
        Me.dgAddQty.Name = "dgAddQty"
        Me.dgAddQty.ReadOnly = true
        Me.dgAddQty.Width = 70
        '
        'dgFdrawno
        '
        Me.dgFdrawno.DataPropertyName = "fdrawno"
        Me.dgFdrawno.HeaderText = "fdrawno"
        Me.dgFdrawno.Name = "dgFdrawno"
        Me.dgFdrawno.Visible = false
        '
        'dgFsono
        '
        Me.dgFsono.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFsono.DataPropertyName = "fsono"
        Me.dgFsono.HeaderText = "SO"
        Me.dgFsono.Name = "dgFsono"
        Me.dgFsono.Visible = false
        '
        'dgFinumber
        '
        Me.dgFinumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFinumber.DataPropertyName = "finumber"
        Me.dgFinumber.HeaderText = "Item Num"
        Me.dgFinumber.Name = "dgFinumber"
        Me.dgFinumber.Visible = false
        '
        'dgFrelease
        '
        Me.dgFrelease.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFrelease.DataPropertyName = "frelease"
        Me.dgFrelease.HeaderText = "Rel"
        Me.dgFrelease.Name = "dgFrelease"
        Me.dgFrelease.Visible = false
        '
        'dgFpartno
        '
        Me.dgFpartno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.dgFpartno.DataPropertyName = "fpartno"
        Me.dgFpartno.HeaderText = "Part"
        Me.dgFpartno.Name = "dgFpartno"
        Me.dgFpartno.Visible = false
        '
        'fabccode
        '
        Me.fabccode.DataPropertyName = "fabccode"
        Me.fabccode.HeaderText = "fabccode"
        Me.fabccode.Name = "fabccode"
        Me.fabccode.Visible = false
        '
        'fnusrqty1
        '
        Me.fnusrqty1.DataPropertyName = "fnusrqty1"
        Me.fnusrqty1.HeaderText = "fnusrqty1"
        Me.fnusrqty1.Name = "fnusrqty1"
        Me.fnusrqty1.Visible = false
        '
        'fnweight
        '
        Me.fnweight.DataPropertyName = "fnweight"
        Me.fnweight.HeaderText = "fnweight"
        Me.fnweight.Name = "fnweight"
        Me.fnweight.Visible = false
        '
        'cube_cnt
        '
        Me.cube_cnt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cube_cnt.DataPropertyName = "cube_cnt"
        Me.cube_cnt.HeaderText = "Cube Cnt"
        Me.cube_cnt.Name = "cube_cnt"
        Me.cube_cnt.ReadOnly = true
        Me.cube_cnt.Width = 76